# $Id$

=pod
=head1 NAME

ConfigManager - Manage configuration file

=head1  SYNOPSIS
 This example show the correct method to create and use an ConfigManager.
 
 #new is a private method. Instead of new() you must use get_instance()
 my $config = get_instance ConfigManager ("./config/config") ;

 print $config->get_param("ftp_url")."\n" ;

 display_users() ;

 sub display_users
 {
   #get_instance return precedent the instance (config file is only
   #read only)
   my $config2 = get_instance ConfigManager () ;

   #When the parameter is a list, the get_param return an array
   #reference
   my @list_users = @{$config2->get_param("users")} ;

   foreach my $user (@list_users)
   {
      print $user."\n" ;
   }
 }

=head1 DESCRIPTION

 This package allows to manage one configuration manager. This manager
 implements the singleton design pattern. It retrieves the values ​​of
 parameters of a configuration file.

 In configuration file, each line which describes a parameter must use
 this structure :
       param_name = 'param_value' 
       or 
       param_name @= 'param_value1' 'param_value2'.

 Example of configuration file :
 ########################################
 #Ensembl database connection
 ########################################
 ensembl_db_host        = 'ensembldb.ensembl.org'
 max_connection_attempt = '3'
 time_between_attempt   = '10' #time between new attempt in seconds


=cut



package ConfigManager ;


#Class's single instance.
my $singleton ;
use FindBin;

=head2 function _new

 Title        : _new 
 Usage        : _new( [$config_file] )
 Prerequisite : none
 Function     : create and return an configManager instance.
 Returns      : ConfigManger
 Args         : $config_file                string Configuration file
                                            path
 Globals      : none

=cut

sub _new
{
	my ($class, $config_file) = @_ ;
	
	#Gérer la valeur par défaut du fichier de configuration à charger
	if (! defined($config_file) )
	{
	 	$config_file = $FindBin::RealBin."/../config/config.txt";	
	}
	my $this = {} ;   
    
	#Rattacher le hachage crée à la classe
	bless $this, $class ;
     
	#Charger les données du fichier de configuration
	$this->load_conf_file($config_file) ;
 
	return $this ;
}


=head2 procedure load_conf_file

 Title        : load_conf_file
 Usage        : load_conf_file( $config_file )
 Prerequisite : none
 Function     : load in the configManager the parameters and  their
                values from the specified configuration file.
 Returns      : none
 Args         : $config_file                string Configuration file
                                            path
 Globals      : none

=cut

sub load_conf_file
{
	my ($this, $config_file) = @_ ;
	
	open(CONFIG_FILE, "<", $config_file) or die "[ERROR] Cannot open ".$config_file."\n" ;
	
	#Tant qu'il y a des ligne dans le fichier de configuration
	while( my $line = <CONFIG_FILE> )
	{
		#Si la ligne contient un paramètre
		if($line =~ /^\s*([^\s]+)\s*@?=\s*'[^']*'/)
		{
			my $param = $1 ;
			
			#Si le paramètre est une variable simple
			if($line =~ /^\s*$param\s*=\s*'([^']*)'/)
			{
				$this->{$param} = $1 ;
			}
			#Si le paramètre est une liste '@='
			else
			{
				my @list = ($line =~ m/'([^']*)'+/g) ;
				$this->{$param} = \@list ;
			}
		}
	}
 
	close(CONFIG_FILE) ;
}


=head2 function get_instance

 Title        : get_instance
 Usage        : get_instance( [$config_file] )
 Prerequisite : none
 Function     : returns an ConfigManager instance. If an instance
                already exists, it return this instance, else it creates
                and return an new single instance.
 Returns      : ConfigManger
 Args         : $config_file                string Configuration file
                                            path
 Globals      : none

=cut

sub get_instance
{
	my ($this, $config_file) = @_ ;
	
	#Si une instance du ConfigManager existe déjà
	if (defined $singleton)
	{
		return $singleton ;
	}
	#Si aucune instance du ConfigManager existe
	else
	{
		$singleton = $this->_new($config_file) ;
		return $singleton ;
	}
}


=head2 function get_param

 Title        : get_param
 Usage        : get_param( $param_name )
 Prerequisite : none
 Function     : returns the value of the requested parameter.
 Returns      : string or array reference
 Args         : $param_name                 string Parameter name
 Globals      : none

=cut

sub get_param
{
	my ($this, $param_name) = @_ ;
	
	#Si le fichier ne contient pas le paramètre
	if( !defined $this->{$param_name} )
	{
		print STDERR "The parameter ".$param_name." does not exist in config file.\n" ;
		return (undef);
	}
	
	return $this->{$param_name} ;
}


1 ;
