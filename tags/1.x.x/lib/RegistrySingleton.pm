# $Id$

=pod
=head1 NAME

RegistrySingleton - Manage ensembl registry and limitation of connexion

=head1  SYNOPSIS

 Prerequisite : packages Core of the Ensembl API, Try::Tiny and ConfigManager

 This example show the correct method to create and use an RegistrySingleton.
 
 #new is a private method. Instead of new() you must use get_registry()
 my $registry = get_registry RegistrySingleton () ;
 $registry->get_adaptor( 'Danio rerio' , 'Core', 'Transcript' ) ;
 
=head1 DESCRIPTION

 This package allows to create a registry on ensembl database. This manager
 implements the singleton design pattern. It manage maximum numbers of call 
 to the function get_adaptor and reload the connexion when it's needed.

=cut



package RegistrySingleton ;


use ConfigManager ;
use Try::Tiny ;
use Bio::EnsEMBL::Registry ;


my $SINGLETON ; #Class's single instance.
my $NB_CALL_GET_ADAPTATOR = 0 ;

=head2 function _new

 Title        : _new 
 Usage        : _new( [$user] )
 Prerequisite : none
 Function     : create and return an RegistrySingleton instance.
 Returns      : RegistrySingleton
 Args         : $user              string User used to connect to
                                   ensembldb.
 Globals      : none

=cut

sub _new
{
	my ($class,$user) = @_ ;

	my $this = {} ;   
    
    #Fixer les attributs du singleton
    my $config                          = get_instance ConfigManager () ;
    $this->{'registry'}                 = 'Bio::EnsEMBL::Registry' ;
	$this->{'max_nb_attempt'}           = $config->get_param("max_connection_attempt") ;
	$this->{'time_between_connections'} = $config->get_param("time_between_attempt") ;
	
	#Rattacher le hachage crée à la classe
	bless $this, $class ;
	
	# Etablir la connexion	
	$this->_connect($user) ;
	
	return $this ;
}


=head2 function get_registry

 Title        : get_registry
 Usage        : $registry = get_registry RegistrySingleton([$user])
 Prerequisite : none
 Function     : returns the connection to the database Ensembl.
 Returns      : RegistrySingleton
 Args         : $user              string User used to connect to
                                   ensembldb.
 Globals      : none

=cut

sub get_registry
{
	my ($class,$user) = @_ ;
	
	#Si une instance du RegistrySingleton existe déjà
	if (defined $SINGLETON)
	{
		return $SINGLETON ;
	}
	#Si aucune instance du RegistrySingleton existe
	else
	{
		$SINGLETON = $class->_new($user) ;
		return $SINGLETON ;
	}
}


=head2 procedure _connect

 Title        : _connect
 Usage        : $this->_connect([user])
 Prerequisite : none
 Function     : connect to the ensembl db.
 Returns      : none
 Args         : $user              string User used to connect to
                                   ensembldb.
 Globals      : none

=cut

sub _connect
{
	my ($this, $user) = @_ ;
	
	#Initialisation des variables
	my $is_connect = 0 ; 
	my $connection_tried = 0 ;	
	$NB_CALL_GET_ADAPTATOR = 0 ;
	
	#Paramètres de connection
	my $config = get_instance ConfigManager () ;
	
		#Si l'utilisateur n'est pas fourni
		if( !(defined $user) )
		{
			$user = $config->get_param("ensembl_db_user") ;
		}
	
		#Récupération des bases de connexion
		$hosts = $config->get_param("ensembl_db_host") ;
		$ports = $config->get_param("ensembl_db_port") ;
	
		#Création de la liste de base de données auxquelles se connecter
		my @ensembl_db_list ;
		
		for( my $host = 0 ; $host < scalar( @{$hosts} ) ; $host++ )
		{
			my $ensembl_db =	{
					-host => $$hosts[$host],
					-port => $$ports[$host],
					-user => $user
				} ;
			
			$ensembl_db_list[$host] = $ensembl_db ;
		}

	#Tant qu'aucune connection n'est établit et que l'on à pas dépassé
	#le nombre de tentative autorisé
	while( $is_connect == 0 && $connection_tried < $this->{'max_nb_attempt'} )
	{
		$connection_tried++ ;

		#Etablir la connection
		try
		{
			$this->{'registry'}->load_registry_from_multiple_dbs(
				@ensembl_db_list
			);
			
			$is_connect = 1 ;
		}
		#Si la connection a échoué
		catch
		{
			#Attendre quelques secondes
			sleep $this->{'time_between_connections'} ;
		}
	}

	#Si à l'issu des tentatives de connection aucune n'a réussi
	if( !$is_connect )
	{
		my $host_list = "" ;
		
		for( my $host = 0 ; $host < scalar( @{$hosts} ) ; $host++ )
		{
			$host_list .= "'".$$hosts[$host]."' or " ;
		}
		
		die "[ERROR] Can't connect on ".substr($host_list, O, -4).".\n" ;
	}	
}


=head2 procedure _disconnect

 Title        : disconnect
 Usage        : $this->_disconnect()
 Prerequisite : none
 Function     : disconnect from ensembl db
 Returns      : none
 Args         : none
 Globals      : none

=cut

sub _disconnect
{
	my $this = shift ;
	$this->{'registry'}->clear();
}


=head2 function get_adaptor

 Title        : get_adaptor
 Usage        : $registry->get_adaptor()
 Prerequisite : none
 Function     : returns an adaptor
 Returns      : adaptor
 Args         : Arg [1]    : name of the species to add the adaptor to in the registry.
  				Arg [2]    : name of the group to add the adaptor to in the registry.
  				Arg [3]    : name of the type to add the adaptor to in the registry.  				
 Globals      : none

=cut

sub get_adaptor
{
	my ($this) = shift ;
	
	return $SINGLETON->{'registry'}->get_adaptor(@_);
}

1 ;
