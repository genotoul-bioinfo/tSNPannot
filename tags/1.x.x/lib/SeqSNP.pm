# $Id$

=pod
=head1 NAME

SeqSNP - Represents an SNP sequence

=head1 DESCRIPTION

 This package allows to represent a SNP with the sequences upstream and
 downstream.

=cut

package SeqSNP ;

use strict ;
use Bio::SeqIO ;



=head2 function alleles

 Title        : alleles
 Usage        : @alleles = alleles()
 Prerequisite : none
 Function     : Returns the list of alleles. The first allele is the
                reference allele.
 Returns      : array
 Args         : none
 Globals      : none

=cut

sub alleles
{
	my $this = shift ;
	return( $this->{ref_allele}, @{$this->{variations}} ) ;
}


=head2 function alleles_string

 Title        : alleles_string
 Usage        : $alleles = alleles_string()
 Prerequisite : none
 Function     : returns the string that represents the set of alleles of
                the sequence. Each allele is separated by a slash.
 Returns      : string
 Args         : none
 Globals      : none

=cut

sub alleles_string
{
	my $this = shift ;
	my $other_alleles = join('/', @{$this->{variations}}) ;
	
	return( $this->{ref_allele}."/".$other_alleles ) ;
}


=head2 function desc

 Title        : desc
 Usage        : $description = desc()
 Prerequisite : none
 Function     : Returns the sequence string.
 Returns      : string
 Args         : none
 Globals      : none

=cut

sub desc
{
	return( shift->{desc} ) ;
}


=head2 function id

 Title        : id
 Usage        : $id = id()
 Prerequisite : none
 Function     : Returns the identifier of the sequence.
 Returns      : string
 Args         : none
 Globals      : none

=cut

sub id
{
	return( shift->{id} ) ;
}


=head2 function length

 Title        : length
 Usage        : $length = length()
 Prerequisite : none
 Function     : Returns the length of the sequence.
 Returns      : int
 Args         : none
 Globals      : none

=cut

sub length
{
	return( length shift->{seq} ) ;
}


=head2 function new

 Title        : new 
 Usage        : new( $seq, $desc, $snp_position, $ref_allele,
                @variations )
 Prerequisite : package Bio::SeqIO
 Function     : Creates an instance of SeqSNP from an Bio::Seq describes
                a SNP.
 Returns      : SeqSNP
 Args         : $seq               Bio::SeqIO An SNP sequence
                
                $desc              string The description of the
                                   sequence
                
                $snp_position      int The position of SNP in the
                                   sequence
                
                $ref_allele        char The reference allele
                
                @variations        array List of alleles other than the
                                   reference allele
 Globals      : none

=cut

sub new
{
	my ( $class, $seq, $desc, $snp_position, $ref_allele, @variations ) = @_ ;
	
	#Renseigner les attributs de la classe
	my $this = {
	               id            =>  $seq->id(),
	               desc          =>  $desc,
	               seq           =>  $seq->seq(),
	               snp_position  =>  $snp_position,
	               ref_allele    =>  $ref_allele,
	               variations    =>  \@variations #The other alleles of the SNP
    	        } ;   
    	        
	#Rattacher le hachage crée à la classe
	bless $this, $class ;

	return $this ;
}


=head2 function ref_allele

 Title        : ref_allele
 Usage        : $allele = ref_allele()
 Prerequisite : none
 Function     : Returns the reference allele.
 Returns      : character
 Args         : none
 Globals      : none

=cut

sub ref_allele
{
	return( shift->{ref_allele} ) ;
}


=head2 function seq

 Title        : seq
 Usage        : $seq = seq()
 Prerequisite : none
 Function     : Returns the sequence string.
 Returns      : string
 Args         : none
 Globals      : none

=cut

sub seq
{
	return( shift->{seq} ) ;
}


=head2 function snp_position

 Title        : snp_position
 Usage        : $position = snp_position()
 Prerequisite : none
 Function     : Returns the position of SNP in the sequence.
 Returns      : int
 Args         : none
 Globals      : none

=cut

sub snp_position
{
	return( shift->{snp_position} ) ;
}

1 ;
