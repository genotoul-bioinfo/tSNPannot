# $Id$

=pod
=head1 NAME

annotationOutput - Functions to write the output of snpAnnotator

=head1 DESCRIPTION

 This package provides functions to write the output of snpAnnotator in
 a XML or TXT file.

=cut

use XML::Writer;
use IO::File;
use Switch ;
use strict ;


=head2 function annotation_to_string

 Title        : annotation_to_string
 Usage        : annotation_to_string( $ref_allele, $hsp_list, $verbose )
 Prerequisite : package coordinates
 Function     : returns the textual output corresponding to a list of
                annotations and/or rejections of HSPs. This list comes
                from snpAnnotator.
 Returns      : none
 Args         : $ref_allele           char Reference nucleotide for the
                                      SNP
                
                $hsp_list             hash reference HSPs with their
                                      annotations
                
                $verbose              binary One if you want the
                                      complete annotation
 Globals      : none

=cut

sub annotation_to_string
{
	my ( $ref_allele, $hsp_list, $verbose ) = @_ ;
	my $layout_level = 1 ;
	my $text ='' ;
	
	#Pour chaque hsp
	foreach  my $hsp_id ( sort{$$hsp_list{$b}{'hsp'}->bits <=> $$hsp_list{$a}{'hsp'}->bits} keys %{$hsp_list} )
	{
		my $hsp = $$hsp_list{$hsp_id} ;
		
		$text .= get_text_layout("Subject protein id ".$$hsp{'protein_id'}, ($layout_level + 1)) ;
	
		#Description du subject de l'alignement
		$text .= get_text_layout("Subject description", $layout_level + 2) ;
		$text .= get_text_layout("Species : ".$$hsp{'species'}, $layout_level + 3) ;
		$text .= get_text_layout("Gene name : ".$$hsp{'gene_name'}, $layout_level + 3) ;
		$text .= get_text_layout("Details : ".$$hsp{'gene_desc'}, $layout_level + 3) ;
	
		#Description de l'alignement
		$text .= alignment_to_string($$hsp{'hsp'}, $verbose, $layout_level + 2) ;


		#Localisation du snp sur l'alignement
		if( $verbose )
		{
			$text .= get_text_layout("SNP position on alignment", $layout_level + 2) ;
			$text .= get_text_layout("Character number : ".$$hsp{"snp_on_alignment"}
								, $layout_level + 3) ;
		}
		
		
		#Si le hsp est rejeté
		if(defined $$hsp{'reasons_rejection'})
		{
			#Message indiquant la raison du rejet de la séquence pour l'annotation
			$text .= get_text_layout("Rejected : ".$$hsp{'reasons_rejection'}, 'advert') ;
		}
		#Si le hsp est annoté
		else
		{	
			#Localisation du snp sur le génome
			$text .= get_text_layout("SNP position on reference genome", $layout_level + 2) ;
			$text .= get_text_layout(
					"Region : ".$$hsp{"snp_on_ref_genome"}->slice()->seq_region_name()
					." Strand : ".$$hsp{"snp_on_ref_genome"}->strand()
					." Nucleic Acid number : ".$$hsp{"snp_on_ref_genome"}->start()
					." Base : ".$$hsp{"base_on_ref_genome"}
					." AA : ".$$hsp{"aa_on_ref_genome"},
					$layout_level + 3) ;
			
			
			#Localisation par rapport aux barrières d'exon
			$text .= get_text_layout("Distance to the limit of the exon", $layout_level + 2) ;
			$text .= get_text_layout(
					"5' limit : ".$$hsp{"five_prime_exon_limit"}, $layout_level + 3) ;		
			$text .= get_text_layout(
					"3' limit : ".$$hsp{"three_prime_exon_limit"}, $layout_level + 3) ;			
					
			
			#Balise sur l'existance d'un SNP de référence
			$text .= get_text_layout("SNP related to the reference genome", $layout_level + 2) ;
			
			if( defined $$hsp{"related_reference_snp"} )
			{
				#Pour chaque SNP de référence localisé au même endroit que notre SNP				
				foreach my $related_snp ( @{$$hsp{"related_reference_snp"}} )
				{
					$text .= get_text_layout($$related_snp{'name'}." ".$$related_snp{'allele'}, $layout_level + 3) ;
				}
			}
			else
			{
				$text .= get_text_layout("There is no reference SNP", $layout_level + 3) ;			
			}
			$text .= get_text_layout("SNP codon position : ".$$hsp{"snp_on_codon"}, $layout_level + 2) ;

			#Conséquences du snp sur l'espèce de référence
			if( $verbose )
			{
				$text .= get_text_layout("SNP consequences on reference transcripts",
									$layout_level + 2) ;
				
				foreach my $allele (keys %{$$hsp{'variation_on_ref'}})
				{
					$text .= get_text_layout("Change in genome ".$allele,  $layout_level + 3) ;
					
					foreach my $trancript (keys %{$$hsp{'variation_on_ref'}{$allele}})
					{
						my $transcript_change = "Change in transcript ".$trancript." : " ;
							
						foreach my $consequence (keys %{$$hsp{'variation_on_ref'}{$allele}{$trancript}})
						{
							$transcript_change .= $$hsp{'variation_on_ref'}{$allele}{$trancript}{$consequence}."|" ;
						}

						chop $transcript_change ;
						$text .= get_text_layout($transcript_change, $layout_level + 4) ;
					}
				}
			}
			
			
			#Conséquences du snp sur l'EST
			$text .= get_text_layout("SNP consequences on translated query "
								."sequence", $layout_level + 2) ;

				#Avertissement sur l'interprétation des résultats
				if( $$hsp{'snp_in_five_prime_utr'} || $$hsp{'frameshift_before_snp'} )
				{
						#Indiquer si il y a un stop avant le snp
						if( $$hsp{'snp_in_five_prime_utr'} )
						{
							$text .= get_text_layout("WARNING : There is a stop codon before the SNP",
									$layout_level + 3) ;
						}
								
						#Indiquer si il y a un frameshift avant le snp
						if( $$hsp{'frameshift_before_snp'} )
						{
							$text .= get_text_layout("WARNING : This consequences are determined "
								."without taking into account frameshift", $layout_level + 3) ;
						}
				}			

				#Conséquence de chaque allèle
				my $aa_reference = $$hsp{'variation_on_protein_query'}{$ref_allele} ;
				foreach my $allele (keys %{$$hsp{'variation_on_protein_query'}})
				{
					#Si l'allèle n'est pas celui de référence
					if( $allele ne $ref_allele )
					{
						my $aa_variation = $$hsp{'variation_on_protein_query'}{$allele} ;					
						my $consequence = consequence_to_string($aa_reference."/".$aa_variation) ;
						$text .= get_text_layout($ref_allele."/".$allele."(".$aa_reference."/".$aa_variation.") => ".$consequence, $layout_level + 3) ;
					}
				}
		}
		
		$text .= "//\n" ;
	}

	return $text ;
}


=head2 procedure close_xml

 Title        : close_xml
 Usage        : close_xml( $output_file, $root_markup[, $encoding] )
 Prerequisite : module XML::Writer
 Function     : closes the root markup of an XML document.
 Returns      : none
 Args         : $output_file         string XML file
               
                $root_markup         string Name of the root markup
               
                $encoding            string Type of encoding
 Globals      : none

=cut

sub close_xml
{
	my ( $output_file, $root_markup, $encoding ) = @_ ;
	
	#Créer l'objet XML::Writer permettant d'écrire au format XML
	my $writer = get_xml_writer(">>", $output_file, $encoding) ;
	
	#Fermer la balise racine du document
	$writer->raw("\n") ;
	$writer->endTag($root_markup) ;
	
	#Vérifit que toutes les balises sont fermées
	$writer->end() ;
	
	#Ferme le flux de sortie vers le fichier XML
	$writer->getOutput->close();
}


=head2 function consequence_to_string

 Title        : consequence_to_string
 Usage        : $text = consequence_to_string( $consequences )
 Prerequisite : none
 Function     : returns the type of consequences for an amino acid
                change : SYNONYMOUS_CODING, or STOP_LOST, or
                STOP_GAINED, or NON_SYNONYMOUS_CODING, or
                UNCHARACTERIZED.
 Returns      : string
 Args         : $consequences      string Change with the form : aa1/aa2
 Globals      : none

=cut

sub consequence_to_string
{
	my ( $consequence ) = @_ ;
	my @alleles = split ('/', $consequence) ;
	
	#Si un des codons n'a pas pu être traduit
	if( ($alleles[0] =~ /N/) || ($alleles[1] =~ /N/) )
	{
		return "UNCHARACTERIZED" ;
	}
	
	#Si les deux acides aminés sont identiques
	if( $alleles[0] eq $alleles[1] )
	{
		return "SYNONYMOUS_CODING" ;
	}
	
	#Si un acide aminé est remplacé par un STOP
	if( $alleles[0] eq '*' )
	{
		return "STOP_LOST" ;
	}
	
	#Si un STOP est remplacé par un acide aminé
	if( $alleles[1] eq '*' )
	{
		return "STOP_GAINED" ;
	}
	
	#Sinon un acide aminé est remplacé par un autre acide aminé
	return "NON_SYNONYMOUS_CODING" ;
}


=head2 function get_text_layout

 Title        : get_text_layout
 Usage        : $text = get_text_layout( $text, $text_level )
 Prerequisite : none
 Function     : returns the text formatted according to the level of
                layout selected.
 Returns      : string
 Args         : $text               string Text
                
                $text_level         int Text formatter level used
 Globals      : none

=cut

sub get_text_layout
{
	my ( $text, $text_level ) = @_ ;
	
	#Selon le niveau de titre souhaité
	switch( $text_level )
	{
		#Mise en forme de niveau 1
		case 1
		{ 
			$text = 
			"###########################################################\n"
			."#\t".$text."\n"
			."##########################################################\n" ;
		}
		#Mise en forme de niveau 2
		case 2 
		{ 
			$text = 
			" ---> ".$text."\n" ;
		}
		#Mise en forme de niveau 3
		case 3 
		{
			$text = 
			"\t".$text."\n" ;
		}
		#Mise en forme de niveau 4
		case 4 
		{
			$text = 
			"\t\t".$text."\n" ;
		}
		#Mise en forme de niveau 5
		case 5
		{
			$text = 
			"\t\t\t".$text."\n" ;
		}
		#Mise en forme des avertissements
		case 'advert'
		{
			$text = 
			"\n\t-------------------------------------------------------------------\n"
			."\t".$text."\n"
			."\t--------------------------------------------------------------------\n" ;
		}
		#Si le niveau de mise en forme n'existe pas
		else
		{	
			die "[ERROR] The layout level $text_level doesn't exist.\n" ;
		}
	}

	return $text ;
}


=head2 function get_xml_writer

 Title        : get_xml_writer
 Usage        : $writer, $output = get_xml_writer( $type_of_write,
                $output_file[, $encoding] )
 Prerequisite : packages IO::File and XML::Writer
 Function     : returns the object XML::Writer which allows to write in
                the specified XML.
 Returns      : XML::Writer
 Args         : $type_of_write       string Open mode of the file > or
                                     >>
                
                $output_file         string XML file
                
                $encoding            string Type of encoding (default :
                                     utf-8)
 Globals      : none

=cut

sub get_xml_writer
{
	my ( $type_of_write, $output_file, $encoding ) = @_ ;
	
	#Gestion du type d'encodage par défaut
	if(!defined $encoding)
	{
		$encoding = "utf-8" ;
	}
	
	#Ouverture du flux de sortie vers le fichier
	my $output = new IO::File( $output_file, $type_of_write );
	
	#Création du writer
	my $writer = new XML::Writer( 
		OUTPUT      => $output,
		DATA_INDENT => 3,             # indentation, trois espaces
		DATA_MODE   => 1,             # changement ligne.
		ENCODING    => $encoding,
		UNSAFE      => 1
	);

	return $writer ;
}


=head2 function alignment_to_string

 Title        : alignment_to_string
 Usage        : $text = alignment_to_string(  $alignment, $verbose, 
                $layout_level  )
 Prerequisite : package coordinates
 Function     : returns in text version the alignment characteristics of
                a HSP
 Returns      : string
 Args         : $alignment           coordinates:Alignment Information
                                     about a HSP
                
                $verbose             binary One if you want the complete
                                     characteristics
                
                $layout_level        int Text formatter level
 Globals      : none

=cut

sub alignment_to_string
{
	my ( $alignment, $verbose, $layout_level ) = @_ ;
	my $align_string = '' ;
							
	#Représentation de l'alignement					
	if( $verbose )
	{
		$align_string .= get_text_layout( "HSP description", $layout_level ) ;	
		my $query = "\t".$alignment->query_string."\t" ;
		my $hit = $alignment->hit_start."\t".$alignment->hit_string."\t".$alignment->hit_end ;
		
		#Si la frame est positive
		if( $alignment->query_strand >= 0 )
		{
			$query = $alignment->query_start.$query.$alignment->query_end ;
		}
		#Si la frame est négative
		else
		{
			#Inverser query_start et query_end
			$query = $alignment->query_end.$query.$alignment->query_start ;
		}
		
		$align_string .= get_text_layout( "Query\t".$query, $layout_level + 1 ) ;
		$align_string .= get_text_layout( "Sbjct\t".$hit , $layout_level + 1 ) ;
	}

	#Statistiques de l'alignement
	$align_string .= get_text_layout( "HSP properties", $layout_level ) ;
	$align_string .= get_text_layout( "Conserved fraction : ".$alignment->frac_conserved, $layout_level + 1 ) ;
	$align_string .= get_text_layout( "Identical fraction : ".$alignment->frac_identical, $layout_level + 1 ) ;
	$align_string .= get_text_layout( "Bit score          : ".$alignment->bits, $layout_level + 1 ) ;
	$align_string .= get_text_layout( "E-value            : ".$alignment->evalue, $layout_level + 1 ) ;
	
	return $align_string ;
}


=head2 procedure open_xml

 Title        : open_xml
 Usage        : open_xml( $output_file, $root_markup[, $encoding] )
 Prerequisite : package XML::Writer
 Function     : écrit l'en-tête et la balise racine d'un fichier XML 
                d'annotation 
 Returns      : none
 Args         : $output_file         string XML file
                
                $root_markup         string Name of the root markup
                
                $encoding            string Type of encoding
 Globals      : none

=cut

sub open_xml
{
	my ( $output_file, $root_markup, $encoding ) = @_ ;
	
	#Créer l'objet XML::Writer permettant d'écrire au format XML
	my $writer = get_xml_writer(">", $output_file, $encoding) ;
	
	#Ecrire l'en-tête xml du fichier
	$writer->xmlDecl();
	
	#Créer la balise racine du document
	$writer->startTag($root_markup);
	
	$writer->getOutput->close();
}


=head2 procedure write_output

 Title        : write_output
 Usage        : write_output( $snp, $hsp_interest, $reject_hsp,
                $show_rejection, $verbose, $output_file, $output_format)
 Prerequisite : packages XML::Writer for write_xml_output and SeqSNP
 Function     : write the annotation of the SNP on the specified file
                with the specified format.
 Returns      : none
 Args         : $snp                  SeqSNP The SNP sequence
                
                $hsp_interest         hash reference HSPs interesting
                                      with their annotations
                
                $reject_hsp           hash reference HSPs which did not
                                      allow to use the reference
                                      information to annotate the
                                      sequence
                
                $output_file          string Output file
                
                $output_format        string Type of output file txt or
                                      xml
                
                $show_rejection       binary One if you want the
                                      rejected_hsp in your results
                
                $verbose              binary One if you want the
                                      detailed version of results
 Globals      : none

=cut

sub write_output
{
	my ($snp, $hsp_interest, $reject_hsp, $output_file, 
	          $output_format, $show_rejection, $verbose) = @_ ;
	
	#Selon le type de sortie demandé
	switch ($output_format)
	{
		#Demande de sortie textuelle
		case 'txt'
		{
			write_txt_output($snp, $hsp_interest, $reject_hsp, 
			                     $output_file, $show_rejection, $verbose) ;
		}
		#Demande de sortie xml
		case 'xml'
		{
			write_xml_output($snp, $hsp_interest, $reject_hsp,
			                    $output_file, $show_rejection, $verbose) ;
		}
		#Si le type de sortie n'est pas reconnu
		else
		{	
			die "[ERROR] ".$output_format." is not a valid output format.\n" ;
		}
	}	
}


=head2 procedure write_txt_output

 Title        : write_txt_output
 Usage        : write_txt_output( $snp, $hsp_interest, $reject_hsp,
                $output_file, $show_rejection, $verbose )
 Prerequisite : packages coordinates and SeqSNP
 Function     : write the annotation of the SNP on the specified TXT
                file.
 Returns      : string
 Args         : $snp                  SeqSNP The SNP sequence
                
                $hsp_interest         hash reference HSPs interesting
                                      with their annotations
                
                $reject_hsp           hash reference HSPs which did not
                                      allow to use the reference
                                      information to annotate the
                                      sequence
                
                $output_file          string Output file
                
                $show_rejection       binary One if you want the
                                      rejected_hsp in your results
                
                $verbose              binary One if you want the
                                      detailed version of results
 Globals      : none

=cut

sub write_txt_output
{
	my ($snp, $hsp_interest, $reject_hsp, $output_file, 
	$show_rejection, $verbose) = @_ ;
	my $annotation_text = '' ;

	#Titre
	$annotation_text .= get_text_layout("SNP sequence id : ".$snp->id(), 1) ;
 		
	#Description accompagnant la séquence SNP		
	$annotation_text .= "Description : ".$snp->desc()." ALLELE:".$snp->alleles_string()." SNP:".$snp->snp_position()." LENGTH:".$snp->length()."\n" ;

	if( $show_rejection )
	{
		#Réunir les hsp annotés et rejetés dans un même hachage
		my %hsp ;
		@hsp{keys %{$hsp_interest}} = values %{$hsp_interest} ;
		@hsp{keys %{$reject_hsp}} = values %{$reject_hsp} ;

		#Informations des hsp annotés et rejetés
		$annotation_text .= annotation_to_string($snp->ref_allele, \%hsp, $verbose) ;
	}
	else
	{
		#Informations des hsp annotés
		$annotation_text .= annotation_to_string($snp->ref_allele, $hsp_interest, $verbose) ;
	}
	
	#Ecrire les annotations
	open (ANNOT_FILE, ">>", $output_file) or die "[ERROR] Can't create ".$output_file.".\n" ;
	print ANNOT_FILE $annotation_text ;
	close ANNOT_FILE ;
}


=head2 procedure write_xml_annotation

 Title        : write_xml_annotation
 Usage        : write_xml_annotation( $writer, $ref_allele, $hsp_list,
                $verbose )
 Prerequisite : packages XML::Writer and coordinates
 Function     : write in xml format the information corresponding to a
                list of annotations and/or rejections of HSPs. This list
                comes from snpAnnotator.
 Returns      : none
 Args         : $writer               XML::Writer Object which allows to
                                      write in the XML file
                
                $ref_allele           char Reference nucleotide for the
                                      SNP
                
                $hsp_list             hash reference HSPs with their
                                      annotations
                
                $verbose              binary One if you want the
                                      detailed version of results
 Globals      : none

=cut

sub write_xml_annotation
{
	my ( $writer, $ref_allele, $hsp_list, $verbose ) = @_ ;
	
	#Pour chaque hsp
	foreach  my $hsp_id ( sort{$$hsp_list{$b}{'hsp'}->bits <=> $$hsp_list{$a}{'hsp'}->bits} keys %{$hsp_list} )
	{
		my $hsp = $$hsp_list{$hsp_id} ;

		$writer->startTag('hsp', 'id' => $hsp_id) ;		


		#Balise de description du subject
		$writer->startTag('subject') ;
			$writer->dataElement( 'species', $$hsp{'species'} ) ;
			$writer->dataElement( 'protein', $$hsp{'protein_id'} ) ;
			$writer->dataElement( 'gene_name', $$hsp{'gene_name'} ) ;
			$writer->dataElement( 'gene_desc', $$hsp{'gene_desc'} ) ;
		$writer->endTag('subject') ;	
		

		#Balise de caractérisation de l'alignement
		write_xml_alignment($$hsp{'hsp'}, $$hsp{'snp_on_alignment'}, $verbose, $writer) ;


		#Balise sur l'annotation de la séquence SNP
		my $annotation_status = ( (defined $$hsp{'reasons_rejection'}) ? 'rejected' : 'valid' ) ;
		$writer->startTag('annotation', 'status' => $annotation_status) ;
				
		if(defined $$hsp{'reasons_rejection'})
		{
			#Balise indiquant la raison du rejet de la séquence pour l'annotation
			$writer->dataElement('reasons_for_rejection', $$hsp{'reasons_rejection'}) ;
		}	
		else
		{
			#Balise de localisation du snpnsur le génome de référence
			$writer->startTag('snp_genome_location') ;
				$writer->dataElement('region', $$hsp{'snp_on_ref_genome'}->slice()->seq_region_name()) ;
				$writer->dataElement('strand', $$hsp{'snp_on_ref_genome'}->strand) ;
				$writer->dataElement('position', $$hsp{'snp_on_ref_genome'}->start) ;
				$writer->dataElement('base', $$hsp{'base_on_ref_genome'}) ;
				$writer->dataElement('aa', $$hsp{'aa_on_ref_genome'}) ;
			$writer->endTag('snp_genome_location') ;
			
			
			#Balise sur les limites de l'exon
			$writer->startTag('distance_exon_limits') ;	
				$writer->dataElement('five_prime_limit', $$hsp{'five_prime_exon_limit'}) ;
				$writer->dataElement('three_prime_limit', $$hsp{'three_prime_exon_limit'}) ;
			$writer->endTag('distance_exon_limits') ;
			
			
			#Balise sur l'existance d'un SNP de référence
			if( defined $$hsp{'related_reference_snp'} )
			{
				$writer->startTag('ref_related_snps') ;
				
				foreach my $related_snp ( @{$$hsp{'related_reference_snp'}} )
				{
					$writer->dataElement('related_snp', $$related_snp{'allele'}, 'id', $$related_snp{'name'}) ;
				}
				
				$writer->endTag('ref_related_snps') ;
			}


			#Balise sur les conséquences du snp
			$writer->startTag('variation') ;
				
				#Conséquences sur l'espèce de référence
				if ( $verbose )
				{
					$writer->startTag('on_ref') ;
						
						#Pour chaque allèle
						foreach my $allele (keys %{$$hsp{'variation_on_ref'}})
						{
							$writer->startTag('mutation', 'desc' => $allele) ;
							
							#Pour chaque transcrit affecté par ce changement d'allèle		
							foreach my $trancript (keys %{$$hsp{'variation_on_ref'}{$allele}})
							{
								$writer->startTag('consequence', 'transcript_id' => $trancript) ;
									
								my $snp_conseq = '' ;
								foreach my $consequence (keys %{$$hsp{'variation_on_ref'}{$allele}{$trancript}})
								{
									$snp_conseq .= $$hsp{'variation_on_ref'}{$allele}{$trancript}{$consequence}.'|' ;
								}
								chop $snp_conseq ;
								$writer->characters($snp_conseq) ;
									
								$writer->endTag('consequence') ;
							}
							
							$writer->endTag('mutation') ;
						}
					$writer->endTag('on_ref') ;
				}
				
				#Conséquences sur la séquence SNP
				$writer->startTag('on_snp_seq') ;
					
					#Balise warning
					if( $$hsp{'snp_in_five_prime_utr'} || $$hsp{'frameshift_before_snp'} )
					{
						$writer->startTag('warning') ;
						
						#Indiquer si il y a un stop avant le snp
						if( $$hsp{'snp_in_five_prime_utr'} )
						{
							$writer->dataElement('stop_before', 'There is a stop codon before the SNP') ;
						}
						
						#Indiquer si il y a un frameshift avant le snp
						if( $$hsp{'frameshift_before_snp'} )
						{
							$writer->dataElement('frameshift_before', 'This consequences are determined without taking into account frameshift') ;
						}
						
						$writer->endTag('warning') ;
					}

					#Balises allèle
					my $aa_reference = $$hsp{'variation_on_protein_query'}{$ref_allele} ;
					foreach my $allele (keys %{$$hsp{'variation_on_protein_query'}})
					{
						if( $allele ne $ref_allele )
						{
							my $aa_variation = $$hsp{'variation_on_protein_query'}{$allele} ;					
							my $consequence = consequence_to_string($aa_reference.'/'.$aa_variation) ;
							$writer->dataElement('allele', $consequence, 'desc', $ref_allele.'/'.$allele, 'aa', $aa_reference.'/'.$aa_variation) ;
						}
					}
				$writer->endTag('on_snp_seq') ;	
			$writer->endTag('variation') ;
		}
		$writer->dataElement('codon_position', $$hsp{'snp_on_codon'}) ;
		$writer->endTag('annotation') ;		
		$writer->endTag('hsp') ;
	}
}


=head2 procedure write_xml_alignment

 Title        : write_xml_alignment
 Usage        : write_xml_alignment(  $alignment, $snp_location,
                $verbose, $writer  )
 Prerequisite : packages coordinates and XML::Writer
 Function     : write the elements of XML describing the alignment.
 Returns      : none
 Args         : $alignment           coordinates:Alignment Information
                                     about a HSP
                
                $snp_location        int localisation du snp sur le hsp
                
                $verbose             binary One if you want the complete
                                     characteristics
                
                $writer              XML::Writer Object which allows to
                                     write in the XML file
 Globals      : none

=cut

sub write_xml_alignment
{
	my ( $alignment, $snp_location, $verbose, $writer ) = @_ ;
							
	$writer->startTag("alignment") ;

	#En mode verbeux fournir l'alignement					
	if( $verbose )
	{
		$writer->startTag("description") ;
			$writer->dataElement('query_start', $alignment->query_start) ;
			
			$writer->dataElement('query_end', $alignment->query_end) ;

			$writer->dataElement('query_strand',$alignment->query_strand) ;
		
			$writer->dataElement('hit_start', $alignment->hit_start) ;

			$writer->dataElement('hit_end', $alignment->hit_end) ;

			$writer->dataElement('query_string', $alignment->query_string) ;

			$writer->dataElement('hit_string', $alignment->hit_string) ;
			
			$writer->dataElement('snp_location', $snp_location) ;
		$writer->endTag("description") ;
	}

	#Balise sur les statistiques de l'alignement
	$writer->startTag("properties") ;
		$writer->dataElement('frac_identical', $alignment->frac_identical) ;
			
		$writer->dataElement('frac_conserved', $alignment->frac_conserved) ;

		$writer->dataElement('evalue', $alignment->evalue) ;

		$writer->dataElement('bits', $alignment->bits) ;
	$writer->endTag("properties") ;

	$writer->endTag( "alignment" ) ;
}


=head2 procedure write_xml_output

 Title        : write_xml_output
 Usage        : write_xml_output( $snp, $hsp_interest, $reject_hsp,
                $output_file, $show_rejection, $verbose )
 Prerequisite : packages coordinates, SeqSNP and XML::Writer
 Function     : write the annotation of the SNP on the specified XML
                file.
 Returns      : string
 Args         : $snp                  SeqSNP The SNP sequence
                
                $hsp_interest         hash reference HSPs interesting
                                      with their annotations
                
                $reject_hsp           hash reference HSPs which did not
                                      allow to use the reference
                                      information to annotate the
                                      sequence
                
                $output_file          string Output file
                
                $show_rejection       binary One if you want the
                                      rejected_hsp in your results
                
                $verbose              binary One if you want the
                                      detailed version of results
 Globals      : none

=cut

sub write_xml_output
{
	my ($snp, $hsp_interest, $reject_hsp, $output_file, 
	          $show_rejection, $verbose) = @_ ;
	
	#Créer l'objet XML::Writer permettant d'écrire au format XML
	my $writer = get_xml_writer(">>", $output_file) ;
	
	#Commencer sur une nouvelle ligne
	$writer->raw("\n") ;
	
	#Balise query
	$writer->startTag('query', 'id' => $snp->id());
		
		#Sous-balise description
		$writer->startTag('desc') ;
			$writer->dataElement('text', $snp->desc()) ;
			$writer->dataElement('length', $snp->length()) ;
			$writer->dataElement('snp_alleles', $snp->alleles_string()) ;
			$writer->dataElement('snp_position', $snp->snp_position()) ;
		$writer->endTag('desc') ;
		
		#Sous-balises hits	
		if( $show_rejection )
		{
			#Réunir les hsp annotés et rejetés dans un même hachage
			my %hsp ;
			@hsp{keys %{$hsp_interest}} = values %{$hsp_interest} ;
			@hsp{keys %{$reject_hsp}} = values %{$reject_hsp} ;
			
			#Ecrire les informations des hsp annotés et rejetés
			write_xml_annotation($writer, $snp->ref_allele, \%hsp, $verbose) ;		
		}
		else
		{
			#Ecrire les informations des hsp annotés
			write_xml_annotation($writer, $snp->ref_allele, $hsp_interest, $verbose) ;
		}

	$writer->endTag("query") ;
	
	#Fermer le flux de sortie
	$writer->getOutput->close();
}


1 ;
