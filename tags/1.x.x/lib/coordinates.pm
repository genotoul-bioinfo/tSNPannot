# $Id$

=pod
=head1 NAME

coordinates - Functions to calculate and check the coordinates of an 
              element of a sequence 

=head1  SYNOPSIS

 use coordinates ;
 
 ...
 
 #Convert the HSP extracted from a blast-x file 
 $hsp = blast_hsp_to_alignment($hsp) ;
 
 #Calculate the position of SNP on the alignement
 my ($position_on_align, $position_on_codon, $msg) = 
         get_alignment_position_by_query_position($snp_position, $hsp) ;
 
 my $aa_position ;
 #If SNP is on HSP
 if($msg eq '')
 {
    #Calculate the position of SNP on the protein
    ($aa_position, $msg) = 
         get_position_on_subject($position_on_align, $hsp) ;
 }
		
=head1 DESCRIPTION

 This package provides functions to calculate and project an position
 from a sequence.
 You can obtain :
    - from an alignment, the position on protein subject from a position
      on cDNA query,
    - the position on transcript from a position on a protein,
    - the position on genome from a position on a transcript,
    - the distance between position on genome and limit of nearest exon,
    - the distance between the specified position on transcript and
      exon's limits.

=cut

use Bio::EnsEMBL::Registry ;
use strict ;
use Class::Struct ;


=head2 structure Alignment

 Title        : Alignment
 Function     : Stores information about a HSP.
                Note about format: 
                      insertion representation :
                query      \
                subject    -
                      deletion representation :
                query      /
                subject    -
                      conservation representation :
                query      A
                homology   :
                subject    A
                      identity representation :
                query      V
                homology   .
                subject    A
 Properties   : query_string         query string from alignment
                
                hit_string           hit string from alignment
                
                homology_string      string from alignment
                
                query_start          min position from query alignment
                
                query_end            max position from query alignment
                
                hit_start            min position from hit alignment
                
                hit_end              max position from hit alignment
                
                query_strand         strand of the query
                
                evalue               e-value of the alignment
                
                bits                 bit scrore of the alignment
                
                frac_conserved       fraction conserved on alignment
                                     between hit and query 
                
                frac_identical       fraction identical on alignment
                                     between hit and query 
 Globals      : none

=cut

struct ( Alignment => [
		query_string    =>    '$',
		hit_string      =>    '$',
		homology_string =>    '$',
		query_start     =>    '$',
		query_end       =>    '$',
		hit_start       =>    '$',
		hit_end         =>    '$',
		query_strand    =>    '$',
		evalue          =>    '$',
		bits            =>    '$',
		frac_conserved  =>    '$',
		frac_identical  =>    '$'
]) ;



=head2 function blast_hsp_to_alignment

 Title        : blast_hsp_to_alignment
 Usage        : $hsp_alignment = blast_hsp_to_alignment( $hsp )
 Prerequisite : package Search::HSP::HSPI
 Function     : returns a structure coordinates::Alignment from an
                object HSP::HSPI.
 Returns      : struct coordinates::Alignment
 Args         : $hsp           Search::HSP::HSPI Object HSPI which will
                               be convert
 Globals      : none

=cut
#####fouiller par là du coté des query_string
sub blast_hsp_to_alignment
{	
	my $hsp = $_[0] ;
	my @hit = split('', $hsp->hit_string()) ;	
	#Pour chaque frameshfit dans la query
	while ($hsp->query_string() =~ m/([\\\/])/g)
	{
		#Localiser le frameshift
		my $frameshift_position = (pos $hsp->query_string()) - 1 ;		
		#Remplacer la position correspondante dans le subject par '-'
		$hit[$frameshift_position] = '-' ;
	}
	my $hit_string = join ('', @hit) ;

	#Convertir la notation des frameshift dans la query
	my $query_string = $hsp->query_string() ;
	$query_string =~ tr/\/\\/\\\// ;	
	#Convertir la notation de la chaîne d'homologie
	my $homology_string = $hsp->homology_string() ;
	$homology_string =~ tr/\+[A-Za-z]/\.:/ ;	
	#Créer la structure coordinates::Alignment
	my $hsp_alignment = Alignment->new(
					query_string    =>	$query_string,
					hit_string      =>	$hit_string,
					homology_string =>	$homology_string,
					query_start     =>	$hsp->start('query'),
					query_end       =>	$hsp->end('query'),
					hit_start       =>	$hsp->start('hit'),
					hit_end         =>	$hsp->end('hit'),
					query_strand    =>	$hsp->strand('query'),
					evalue          =>	$hsp->evalue(),
					bits            =>	$hsp->bits(),
					frac_conserved  =>	$hsp->frac_conserved(),
					frac_identical  =>	$hsp->frac_identical()
					);
					
	return $hsp_alignment ;
}


=head2 function border_regions_is_conserved

 Title        : border_regions_is_conserved
 Usage        : $boolean = border_regions_is_conserved(
                $position_on_align, $hsp, $conserved_region_size )
 Prerequisite : none
 Function     : returns a binary indicating if the region of width 
                $conserved_region_size is conserved between query and
                subject around position $position_on_align.
 Returns      : binary
 Args         : $position_on_align         int Position on the alignment
                                           around which we check the
                                           conservation
                
                $hsp                       coordinates::Alignment One
                                           HSP created from an alignment
                                           between an cDNA and a protein
                                      
                $conserved_region_size     int Number of elements that
                                           must be conserved on the
                                           alignment upstream and
                                           downstream the aa encoded by
                                           the SNP
 Globals      : none

=cut

sub border_regions_is_conserved
{
	my ($position_on_align, $hsp, $conserved_region_size) = @_ ;

	#Calculer les positions de début et de fin de la région qui doit être
	#conservé autour du SNP
	my $start_conserved_sequence = $position_on_align - $conserved_region_size ;
	my $end_conserved_sequence   = $position_on_align + $conserved_region_size ;

	#Si l'alignement n'est pas assez étendu pour couvrir toute la région
	#de conservation
	if( $start_conserved_sequence < 1 || 
			$end_conserved_sequence > length $hsp->homology_string )
	{
		return 0 ;
	}	
	
	#Pour chaque position de la région de conservation
	for(my $i = $start_conserved_sequence - 1 ; $i < $end_conserved_sequence ; $i++)
	{
		#Si le caractère n'est pas celui correspondant au SNP
		if( $i != $position_on_align - 1 )
		{
			my $homology_char = substr($hsp->homology_string, $i, 1) ;
			my $query_char = "NA";
			#le snp n'est pas en premiere position ni en derniere position dans la query
			if ( $i >= 0 && $i <= length($hsp->query_string) )
			{
				$query_char    = substr($hsp->query_string, $i, 1) ;
			}
			#Si l'homologie n'indique ni une identité ni une conservation et
			#que l'on est pas dans une zone de faible complexité
			if($homology_char eq ' ' && $query_char ne 'X')
			{
				#Indiquer que la conservation n'est pas respectée
				return 0 ;
			}
		}
	}

	return 1 ;
}


=head2 function convert_aa_in_transcript_first_position

 Title        : convert_aa_in_transcript_first_position
 Usage        : $position = convert_aa_in_transcript_first_position(
                $transcript, $aa_position )
 Prerequisite : package Core of the Ensembl API
 Function     : returns the position on transcript for the first
                nucleotide coding the specified amino acid.
 Returns      : int
 Args         : $transcript         EnsEMBL::Transcript The transcript
                
                $aa_position        int Position of the amino acid in
                                    the protein
 Globals      : none

=cut

sub convert_aa_in_transcript_first_position
{
	my ( $transcript, $aa_position ) = @_ ;
	
	my $aa_cds_position = ($aa_position - 1) * 3 + 1 ;
	my $utr_length = $transcript->cdna_coding_start() - 1 ; #BUG ENSEMBL (correction : $transcript->five_prime_utr->length mais cela est plus long)
	my $transcript_position =  $aa_cds_position + $utr_length ;

	return $transcript_position ;
}


=head2 function exist_before_position

 Title        : exist_before_position
 Usage        : $binary = exist_before_position( $position, $string,
                $charac )
 Prerequisite : none
 Function     : returns a binary that equals 1 if the specified
                character exists before the indicated position on
                string.
 Returns      : binary
 Args         : $position               int Position on the string
                
                $string                 string String where character is
                                        searched
                
                $charac                 string Regex representing the
                                        desired character
 Globals      : none

=cut

sub exist_before_position
{
	my ($position, $string, $charac) = @_ ;
	
	#Récupérer la partie de la chaîne de caractères précédent la position
	my $left_string = substr( $string, 0, $position - 1 ) ;

	#Si le caractère existe en amont de la position
	if( $left_string =~ /$charac/ )
	{
		return 1 ;
	}
	else
	{
		return 0 ;
	}
}


=head2 function fasta_hsp_to_alignment

 Title        : fasta_hsp_to_alignment
 Usage        : $hsp_alignment = fasta_hsp_to_alignment( $hsp )
 Prerequisite : package Search::HSP::HSPI
 Function     : returns a struct coordinates::Alignment from a
                Search::HSP::HSPI object. This object must be generated
                from the fasta program.
 Returns      : struct coordinates::Alignment
 Args         : $hsp           Search::HSP::HSPI Object HSPI which will
                               be convert
 Globals      : none

=cut

sub fasta_hsp_to_alignment
{	
	my $hsp = $_[0] ;

	my $hsp_alignment = Alignment->new(
					query_string    =>	$hsp->query_string(),
					hit_string      =>	$hsp->hit_string(),
					homology_string =>	$hsp->homology_string(),
					query_start     =>	$hsp->start('query'),
					query_end       =>	$hsp->end('query'),
					hit_start       =>	$hsp->start('hit'),
					hit_end         =>	$hsp->end('hit'),
					query_strand    =>	$hsp->strand('query'),
					evalue          =>	$hsp->evalue(),
					bits            =>	$hsp->bits(),
					frac_conserved  =>	$hsp->frac_conserved(),
					frac_identical  =>	$hsp->frac_identical()
					);
					
	return $hsp_alignment ;
}


=head2 get_alignment_position

 Title        : get_alignment_position
 Usage        : ( $position_on_alignment, $position_on_aa ) = 
                get_alignment_position( $nb_nucleic_acids, $sequence )
 Prerequisite : none
 Function     : returns a list. The first element is the position on the
                alignment of the character corresponding to the
                specified nucleotide. The second one is the position of
                this nucleotide on its codon.
 Returns      : int, int
 Args         : $nb_nucleic_acids    int Number of nucleotides between
                                     the start of the alignment and the
                                     nucleotide which be treated
                
                $sequence            string Proteic sequence of the
                                     aligned part of the RNA
 Globals      : none

=cut

sub get_alignment_position
{
	my ($nb_nucleic_acids, $sequence)   = @_ ;
	
	my $nb_nucleic_acids_remaining = $nb_nucleic_acids ;
	my $position_on_alignment      = 1 ;

	#Vérifier que la position demandé est correcte
	if( $nb_nucleic_acids_remaining <= 0 )
	{
		die "[ERROR] The position $nb_nucleic_acids can not exist in a query.\n" ;
	}
	
	#Tant que l'on à pas parcouru la séquence jusqu'à la position 
	#demandé (position comprise) ou que l'on ne dépasse pas la séquence
	for(; $nb_nucleic_acids_remaining > 0 
			&& (length $sequence) >= $position_on_alignment ; 
			$position_on_alignment++ )
	{
		my $carac_at_iteration_position = substr($sequence, $position_on_alignment - 1, 1) ;
	
		#Si le caractère correspond à un acide aminée ou un stop, on 
		#retire 3 au nombre d'acides nucléiques à passer avant d'arriver 
		#au SNP puisque ces éléments sont codés par 3 acides nucléiques
		if( $carac_at_iteration_position =~ /[A-Z*]/i ) 
		{
			$nb_nucleic_acids_remaining -= 3 ;
		}
		#Si le caractère indique une délétion, on retire 1 au nombre 
		#d'acides nucléiques à passer avant d'arriver au SNP. du fait
		#de l'alignement avec une séquence contenant un acide nucléique 
		#de plus, le SNP est décalé un acide nucléique plus loin
		elsif( $carac_at_iteration_position eq '/' )
		{
			$nb_nucleic_acids_remaining += 1 ;#Pour blast on aurait une insertion $nb_nucleic_acids_remaining -= 1 ;
		}
		#Si le caractère correspond à une insertion, on retire 1
		#au nombre d'acides nucléiques à passer avant d'arriver au SNP
		#puisque \ représente un acide nucléique
		elsif( $carac_at_iteration_position eq "\\")
		{
			$nb_nucleic_acids_remaining -= 1 ;#Pour blast on aurait une délétion $nb_nucleic_acids_remaining += 1 ;
		}
		#Sinon le caractère correspond à un gap est ne représente donc 
		#aucun acide nucléique
	}
	
	#Si l'on est sortie de la boucle car la séquence ne contient pas le 
	#nombre d'acides nucléiques indiqué
	if( $nb_nucleic_acids_remaining > 0 )
	{
		$position_on_alignment = "NA" ;
	}
	
	if ($position_on_alignment ne "NA")
	{
		$position_on_alignment -= 1 ;
	}
	
	#Calculer la position de l'acide nucléique dans le codon identifié 
	#par $position_on_alignment
	my $position_on_aa = 3 + $nb_nucleic_acids_remaining ;
		
	return ($position_on_alignment, $position_on_aa) ;
}


=head2 get_alignment_position_by_query_position

 Title        : get_alignment_position_by_query_position
 Usage        : ( $position_on_alignment, $position_on_aa, $msg ) =
                get_alignment_position_by_query_position( 
                $nb_nucleic_acids, $sequence )
 Prerequisite : none
 Function     : returns a list. If the indicated position is not in the
                alignment region, the two first elements are -1 and the
                third element is the error message. Otherwise the first
                number is the position on the alignment of the character
                corresponding to the specified nucleotide. The second
                one is the position of this nucleotide on its codon.

 Returns      : int, int, string
 Args         : $position_on_query     int Position on the query of the
                                       nucleotide
                
                $hsp                   coordinates::Alignment One HSP
                                       created from an alignment
                                       between an cDNA and a protein
 Globals      : none

=cut

sub get_alignment_position_by_query_position
{
	my ($position_on_query, $hsp) = @_ ;

	
	#Si la position du SNP n'est pas dans la zone aligné
	if(!is_in_hsp($position_on_query, $hsp, 'query'))
	{
		return (-1, -1, "Indicated position is not in the Hsp.") ;
	}
	
	
	#Calculer le nombre de nucléotides entre le SNP et le début de 
	#l'alignement (ces deux positions sont comprise dans le nombre)
	my $nb_nucleo_to_position ;
		#Si la frame est positive
		if( $hsp->query_strand >= 0 )
		{
			#nbAcNuSansGap = positionSurLaSeq - positionDebutAlignQuery + 1
			$nb_nucleo_to_position = $position_on_query - $hsp->query_start + 1 ;
		}
		#Si la frame est négative
		else
		{
			#nbAcNuSansGap = positionDebutAlignQuery - positionSurLaSeq + 1
			$nb_nucleo_to_position = $hsp->query_end - $position_on_query + 1 ;
		}
	
	#Calculer la position du SNP dans l'alignement
	my ($position_on_align, $position_on_codon) = 
					get_alignment_position($nb_nucleo_to_position, $hsp->query_string) ;
					
	return ($position_on_align, $position_on_codon, '') ;
}


=head2 function get_genome_position

 Title        : get_genome_position
 Usage        : $feature_position = get_genome_position( $transcript,
                $position_on_trancript )
 Prerequisite : package Core of the Ensembl API
 Function     : returns an object corresponding to the position on
                genome of the indicated position on transcript.
 Returns      : EnsEMBL::Feature
 Args         : $transcript               EnsEMBL::Transcript The
                                          transcript
                
                $position_on_trancript    int Position on the transcript
 Globals      : none

=cut

sub get_genome_position
{
	my ( $transcript, $position_on_trancript ) = @_ ;
	my @exons = @{ $transcript->get_all_Exons() } ;

	foreach my $exon (@exons)
	{
		#Si la position indiqué se trouve entre les limites de l'exon
		if( $exon->cdna_end($transcript) >= $position_on_trancript 
			&& $exon->cdna_start($transcript) <= $position_on_trancript )
		{		
			#Cacul de la localisation
			my $position_on_genome  ;
				#Si l'exon est sur le brin +
				if( $exon->strand() >= 0)
				{
					$position_on_genome = ($exon->start() 
					                    + $position_on_trancript 
					                    - $exon->cdna_start($transcript)) ;
				}
				#Si l'exon est sur le brin -
				else
				{
					$position_on_genome = $exon->end() 
					                    - ($position_on_trancript 
					                    - $exon->cdna_start($transcript)) ;
				}
		
			#Création de l'objet de l'API correspondant à la position 
			#sur le génome
			my $feature = new Bio::EnsEMBL::Feature(
				  -start  => $position_on_genome,
				  -end    => $position_on_genome,
				  -strand => $exon->strand(),
				  -slice  => $exon->slice
				);

			return $feature ;
		} 
	}

	#La position n'a pas été trouvé
	die "[ERROR] The position $position_on_trancript is wrong in transcript "
		.$transcript->stable_id.". Make sure you have the latest version of the Ensembl API. \n" ;
}


=head2 function get_nb_to_position

 Title        : get_nb_to_position
 Usage        : $nb = get_nb_to_position( $position, $sequence, 
                $search_carac )
 Prerequisite : none
 Function     : returns the number of occurrence of the character in the
                sequence before and at the specified position
 Returns      : int
 Args         : $position        int Position on the sequence
                
                $sequence        string Sequence on which we count the
                                 number of searched character
                                 
                $search_carac    char Character which is searched
 Globals      : none

=cut

sub get_nb_to_position
{
	my ($position, $sequence, $search_carac) = @_ ;
	
	my $nb_search_carac_before_position = 0 ;
	
	#Vérifier que la position demandé existe sur la séquence
	if( $position > length($sequence) )
	{
		return "NA" ;
	}
	
	#Tant que l'on à pas parcouru la séquence jusqu'à la position 
	#demandé (position comprise)
	for( my $i = 0 ; $i < $position ; $i++ )
	{
		#Si la lettre de l'acide aminée à la position correspond à au
		#caractère à compter
		if( substr($sequence, $i, 1) eq $search_carac )
		{
			$nb_search_carac_before_position++ ;
		}
	}
	
	return $nb_search_carac_before_position ;
}


=head2 function get_exon_barrier_distance

 Title        : get_exon_barrier_distance
 Usage        : ($upstream_distance, $downtream_distance) =
                get_exon_barrier_distance( $transcript, $position,
                $direct_align )
 Prerequisite : package Core of the Ensembl API
 Function     : returns the number of nucleotides between the specified
                position and the upstream and downstream limit of its
                exon. The position orientation of upstream and
                downstream depends on the orientation of object relative
                to the exon. 
 Returns      : int, int
 Args         : $transcript         EnsEMBL::Transcript The transcript
                
                $position           int Position on transcript
                
                $direct_align       binary Indicates if the initial
                                    object's orientation is the inverse
                                    of that of exon
 Globals      : none

=cut
		
sub get_exon_barrier_distance
{
	my ($transcript, $position_on_transcript, $direct_align) = @_ ;
	my ($upstream_limit, $downstream_limit) = get_transcript_exon_barrier_distance($transcript, $position_on_transcript) ;
	
	#Si la protéine matche avec le reverse complémentaire de la séquence SNP
	if( !$direct_align )
	{
		my $tmp = $upstream_limit ;
		$upstream_limit = $downstream_limit ;
		$downstream_limit = $tmp ;
	}
	
	return ($upstream_limit, $downstream_limit) ;
}


=head2 function get_position_on_subject

 Title        : get_position_on_subject
 Usage        : ( $position_on_prot, $msg ) = get_position_on_subject(
                $position_on_align, $hsp )
 Prerequisite : none
 Function     : returns the position on the subject from the position on
                alignment. If the position correspond to a GAP, or if
                she correspond to an insert, the first returned value
                will be -1 and the second value will contain the error
                message. Otherwise the function returns a list :
                position of the amino acid and an empty string.

 Returns      : int, string
 Args         : $position_on_align         int Position on alignment
                
                $hsp                       coordinates::Alignment One
                                           HSP created from an alignment
                                           between an cDNA and a protein
 Globals      : none

=cut

sub get_position_on_subject
{
	my ($position_on_align, $hsp) = @_ ;
	if ( length( $hsp->query_string ) < $position_on_align - 1 )
	{
		return ("NA", "") ;
	}
	#Vérifier que le SNP ne correspond pas à une insertion
	if(substr($hsp->query_string, $position_on_align-1, 1) eq "\\")
	{
		return (-1, "Indicated position corresponds to an insert.") ;
	}
	if ( length( $hsp->hit_string ) < $position_on_align - 1 )
	{
		return ("NA", "") ;
	}
	#Vérifier que la position du SNP ne correspond pas à un gap sur le subject 
	if(substr($hsp->hit_string, $position_on_align-1, 1) eq '-')
	{
		return (-1, "Indicated position corresponds to a gap.");
	}
	
	
	#Trouver la position sur la protéine
	#	positionProt = positionDebutAlignHit + positionSurAlign - nbNonAcAm
	my $pos_gap1 = get_nb_to_position($position_on_align, $hsp->hit_string, '-');
	my $pos_gap2 = get_nb_to_position($position_on_align, $hsp->hit_string, ' ');
	if ( $pos_gap1 ne "NA" && $pos_gap2 ne "NA" )
	{
		my $position_on_prot = $hsp->hit_start + $position_on_align - 1  
						- $pos_gap1
						- $pos_gap2;
		return ($position_on_prot, "") ;
	}
	else
	{		
		return ("NA", "") ;
	}
}


=head2 function get_transcript_exon_barrier_distance

 Title        : get_transcript_exon_barrier_distance
 Usage        : ($upstream_distance, $downtream_distance) = 
                get_transcript_exon_barrier_distance( $transcript,
                $position )
 Prerequisite : package Core of the Ensembl API
 Function     : returns the number of nucleotides between the specified
                position on transcript and the upstream and downstream
                limit of its exon.
 Returns      : int, int
 Args         : $transcript         EnsEMBL::Transcript The transcript
                
                $position           int Position on transcript
 Globals      : none

=cut

sub get_transcript_exon_barrier_distance
{
	my ( $transcript, $position ) = @_ ;
	my $upstream_distance ;
	my $downtream_distance ;
	my $find = 0 ;
	
	#Si la position indiqué n'est pas correcte ou n'existe pas sur le transcrit
	if( $position < 1 || $position > $transcript->length() )
	{
		die "[ERROR] Position $position does not exist in ".$transcript->stable_id().".\n" ;
	}
	
	#Récupérer les exons du transcrit
	my @exons = @{ $transcript->get_all_Exons() } ;
	
	#Tant que l'on à pas trouvé l'exon dans lequel se trouve le SNP
	my $i = 0 ;
	while( !$find )
	{
		#Récupérer l'exon suivant
		my $exon = $exons[$i] ;
		
		#Si la position du SNP est entre la position de début et 
		#de fin de l'exon courant
		if( $position >= $exon->cdna_start($transcript) 
			&& $position <= $exon->cdna_end($transcript) )
		{
			$upstream_distance  = $position - $exon->cdna_start($transcript) ;
			$downtream_distance = $exon->cdna_end($transcript) - $position ;
			
			#Indiquer que l'exon contenant le SNP a été trouvé
			$find = 1 ; 
		}
		
		$i++ ;
	}

	return($upstream_distance, $downtream_distance) ;
}


=head2 function is_in_hsp

 Title        : is_in_hsp
 Usage        : $binary = is_in_hsp($position, $hsp, $sequence)
 Prerequisite : none
 Function     : returns 1 if the position is contained in the alignment.
 Returns      : a binary
 Args         : $position        int Position
                
                $hsp             coordinates::Alignment One HSP created
                                 from an alignment between an cDNA and a
                                 protein
                
                $sequence        string Check position on 'query' or on
                                 'subject'
 Globals      : none

=cut

sub is_in_hsp
{
	my ($position, $hsp, $sequence) = @_ ;

	#Si l'on veut savoir si la position se trouve dans la partie aligné
	#de la query
	if( $sequence eq "query" )
	{
		return ( ($position <= $hsp->query_end) &&  
					($position >= $hsp->query_start) ) ;
	}
	#Si l'on veut savoir si la position se trouve dans la partie aligné
	#du subject
	else
	{
		return ( ($position <= $hsp->hit_end) &&  
					($position >= $hsp->hit_start) ) ;
	}
}


=head2 function rna_position_from_protein_position

 Title        : rna_position_from_protein_position
 Usage        : $position = rna_position_from_protein_position(
                $transcript, $aa_position, $position_in_aa)
 Prerequisite : package Core of the Ensembl API
 Function     : returns the position on the RNA of a nucleotide from its
                position on the protein.
 Returns      : int
 Args         : $transcript         EnsEMBL::Transcript The transcript
                
                $aa_position        int Position of the amino acid in
                                    the protein (1 or 2 or 3)
                
                $position_in_aa     int Position of the nucleotide on
                                    her codon
 Globals      : none

=cut

sub rna_position_from_protein_position
{
	my ( $transcript, $aa_position, $position_in_aa ) = @_ ;

	#Calculer la position dans l'ARN du premier nucléotide participant 
	#au codon de l'acide aminé
	my $position_start_aa = convert_aa_in_transcript_first_position($transcript, $aa_position) ;

	#Calculer la position du nucléotide souhaité
	my $rna_position  = $position_start_aa + $position_in_aa - 1 ;

	return $rna_position ;
}


1;
