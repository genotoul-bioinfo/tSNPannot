# $Id$

=pod
=head1 NAME

ensemblUtilities - Core functions to use Ensembl banks

=head1 DESCRIPTION

 Core functions to use Ensembl banks.

=cut


use Switch ;
use strict ;
use Try::Tiny ;
use ConfigManager ;
use RegistrySingleton; 



=head2 function get_first_id

 Title        : get_first_id
 Usage        : $registry = get_first_id( $bank_file )
 Prerequisite : none
 Function     : returns the Ensembl id of the first line of the bank
 Returns      : string
 Args         : $bank_file              string Path to the Ensembl Bank
 Globals      : none

=cut

sub get_first_id
{
	my ($bank_file) = @_ ;
	
	#Récupérer la première ligne de la banque
	open(FILE, $bank_file) or die "[ERROR] Cannot open ".$bank_file.".\n" ;
	my $first_line = <FILE> ;
	close FILE ;
		
	#Si cette ligne contient un identifiant Ensembl
	if($first_line =~ /(ENS\w{4}\d{11}) /)
	{
		return $1 ;
	}
	
	#Si aucun identifiant Ensembl n'a été trouvé
	die "[ERROR] Unable to retrieve the first Ensembl identifier in ".$bank_file.".\n" ;
}


=head2 function get_gene_desc_by_translation

 Title        : get_gene_desc_by_translation
 Usage        : ($gene_name, $gene_desc) = get_gene_desc_by_translation(
                $registry, $species, $translation_id )
 Prerequisite : none
 Function     : returns the name and description of the gene.
 Returns      : array
 Args         : $registry               EnsEMBL::Registry Ensembl
                                        database connection
                
                $species                string Latin name of species
                
                $translation_id         string Ensembl ID of the protein
 Globals      : none

=cut

sub get_gene_desc_by_translation
{
	my ($registry, $species, $translation_id) = @_ ;				
	my $gene_name = "" ;
	my $gene_desc = "" ;

	#Accéder aux données du gène correspondant à la protéine
	my $gene_adaptor = $registry->get_adaptor( $species, "core", "gene" ) ;
	my $gene = $gene_adaptor->fetch_by_translation_stable_id( $translation_id ) ;

	#Si des informations ont pu être récupérées
	if( defined $gene )
	{
		#Récupérer le nom du gène si il est défini
		if( !defined ($gene_name = $gene->external_name()) )
		{
			$gene_name = "" ;
		}
		
		#Récupérer la description du gène si elle existe
		if( !defined ($gene_desc = $gene->description()) )
		{
			$gene_desc = "" ;
		}
	}
	
	return ($gene_name,  $gene_desc) ;
}

=head2 is_in_ensembl_core

 Title        : is_in_ensembl_core
 Usage        : $binary = is_in_ensembl_core( $species )
 Prerequisite : package Core of the Ensembl API
 Function     : returns true if slices, genes and transcripts are
                supported by Ensembl for this species.
 Returns      : binary
 Args         : $species               string Latin name of species
 Globals      : none

=cut

sub is_in_ensembl_core
{
	my ( $species ) = @_ ;
	
	#Se connecter à Ensembl
	my $registry = get_registry RegistrySingleton() ;
	
	#Tenter de se connecter aux données de l'espèce	
	$registry->get_adaptor( $species, 'core', 'slice' ) or return 0;
	$registry->get_adaptor( $species, 'core', 'transcript' ) or return 0;	
	$registry->get_adaptor( $species, 'core', 'gene' ) or return 0;
	
	#Si les données générales sont présentes
	return 1 ;
}


=head2 is_in_ensembl_variation

 Title        : is_in_ensembl_variation
 Usage        : $binary = is_in_ensembl_variation( $species )
 Prerequisite : package Core and Variation of the Ensembl API
 Function     : returns true if variation data are supported by Ensembl
                for this species.
 Returns      : binary
 Args         : $species               string Latin name of species
 Globals      : none

=cut

sub is_in_ensembl_variation
{
	my ( $species ) = @_ ;
	
	#Se connecter à Ensembl
	my $registry = get_registry RegistrySingleton() ;
	#Tenter de se connecter aux données de variation de l'espèce
	$registry->get_adaptor($species, 'variation', 'variationfeature') or return 0;
	
	#Si l'espèce possède des données de variation
	return 1 ;
}


=head2 function is_sp_bank

 Title        : is_sp_bank
 Usage        : $binary = is_sp_bank( $bank_type, $species,
                $bank_file, $registry )
 Prerequisite : package Core of the Ensembl API
 Function     : returns true if the bank belongs to the species
                specified
 Returns      : binary
 Args         : $bank_type             string Type of molecules in the
                                       bank
                
                $species               string Latin name of species
                
                $bank_file             string Path to the Ensembl Bank
                
                $registry              EnsEMBL::Registry Ensembl
                                       database connection
 Globals      : none

=cut

sub is_sp_bank
{
	my ($bank_type, $species, $bank_file, $registry) = @_ ;
	
	#Vérifier que le type de banque demandé est supporté et récupérer
	#le type d'objet Ensembl qui lui est associé
	my $object_type ;
	switch ($bank_type)
	{
		case "pep" 
		{ 
			$object_type = 'translation' ;
		}
		case "rna"
		{ 
			 $object_type = 'transcript' ; 	
		}
		else
		{	
			die "[ERROR] ".$bank_type." This type of bank is not supported by is_sp_bank().\n" ;
		}
	}
	
	#Récupérer l'identifiant de la première séquence de la banque	
	my $ensembl_id = get_first_id($bank_file) ;

	#Récupérer un adaptateur sur le type d'objet contenu dans la banque
	my $adaptor = $registry->get_adaptor( $species, 'core', $object_type )
			or die "[ERROR] ".$species." does not exist in Ensembl bank.\n" ;
	
	#Si l'identifiant ne correspond pas à un objet existant pour l'espèce
	#alors retourner 0 : la banque ne correspond pas à l'espèce
	defined $adaptor->fetch_by_stable_id($ensembl_id) or return 0 ;
	
	return 1 ;
}


1;
