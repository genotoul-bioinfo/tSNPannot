# $Id$

=pod
=head1 NAME

manageSnpFiles - Check and convert files of polymorphism

=head1 DESCRIPTION

This package provides functions for manage files of polymorphism.
Some functions allow you to check if your file has the correct syntax
for a file of polymorphism. Others allow to convert this files in format
usable by similarity sequence search programs (example : fast-all or
blast).

=cut

use strict ;
use Bio::SeqIO ;


=head2 procedure fasta_to_blast_usable

 Title        : fasta_to_blast_usable
 Usage        : fasta_to_blast_usable( $snp_file, 
                $snp_file_formated, $file_format )
 Prerequisite : packages Bio::SeqIO and Bio::Seq
 Function     : creates from a fasta file a file of polymorphism usable
                by blast. In this format sequences correspond to one of
                the alleles, and description indicates the SNP's
                position on the sequence and possible values.
 Returns      : none
 Args         : $snp_file            string File of polymorphism in
                                     Fasta format. The SNP are included
                                     in sequences with this syntax
                                     [nucleic_ac/nucleic_ac]
                
                $snp_file_formated   string Reformatted file of
                                     polymorphism
				
                $file_format         string File format generated
                                     (example : 'Fasta')
 Globals      : none

=cut

sub fasta_to_blast_usable
{
	my ($snp_file, $snp_file_formated, $file_format) = @_ ;

	#Créer un fichier Fasta
	my $out_seq = Bio::SeqIO->new(
	                              -file => ">".$snp_file_formated, 
	                             '-format' => $file_format
	                             );
		
								
	#Ovrir le fichier contenant les séquence SNP
	my $in_seq  = Bio::SeqIO->new(
	                              -file => "$snp_file", 
	                             '-format' => 'Fasta'
	                             );

	#Pour chaque séquence SNP du fichier
	while ( my $snp_sequence = $in_seq->next_seq() ) 
	{
		my $sequence = $snp_sequence->seq() ;
		$sequence =~ s/-//g ;
		my $seq_id = $snp_sequence->id() ;
		my $seq_desc ;
		
		#Si la séquence contient un SNP 
		if( $sequence =~ /([^\[]*)\[/ )
		{
			#Indiquer la position du SNP dans la description
			$seq_desc = "SNP:".( ( length $1 ) + 1 ) ;
			
			#Indiquer les acides nucléiques possibles pour le SNP dans
			#la description
			$sequence =~ /\[(\w(\/\w)+)\]/ ;
			$seq_desc = "ALLELE:".$1." ".$seq_desc ;
			
			#Remplacer par le premier des acides nucléiques possibles
			$sequence =~ s/\[(\w)(\/\w)+\]/$1/ ;
		}
		#Si la séquence ne contient pas de SNP 
		else
		{
			die "[ERROR] The sequence ".$snp_sequence->id()." does not contain SNPs.\n" ;
		}
		
		#Ecrire la séquence reformaté dans le nouveau fichier
		my $snp = Bio::Seq->new( 
								-id   => $seq_id,
								-seq  => $sequence,
								-desc => $snp_sequence->desc()." ".$seq_desc );
								
		$out_seq->write_seq($snp);	
	}

}


=head2 procedure vcf_to_blast_usable

 Title        : vcf_to_blast_usable
 Usage        : vcf_to_blast_usable( $vcf_file, $fasta_file, 
                $output_file)
 Prerequisite : packages Bio::SeqIO and Bio::Seq
 Function     : creates from a fasta file and a vcf file a file of polymorphism usable
                by blast. In this format sequences correspond to one of
                the alleles, and description indicates the SNP's
                position on the sequence and possible values.
 Returns      : none
 Args         : $vcf_file            string File of polymorphism in
                                     vcf format.
                
                $fasta_file	   		  string reference file of
                                     concensus
				
                $output_file         string Ouput file path
 Globals      : none

=cut

sub vcf_to_blast_usable
{
	my ($vcf_file, $fasta_file, $output_file) = @_ ;
	my $nb_base_flanking=100;
	#Créer un fichier Fasta
	my $out_seq = Bio::SeqIO->new(
	                              -file => ">".$output_file, 
	                             '-format' => 'Fasta'
	                             );
		
								
	#Ovrir le fichier contenant les séquence de reference
	open (IN, $fasta_file);
	my %h_seq;
	my $seq="";
	my $id="";
	while (<IN>)
    {
    	chomp;
    	if ((my $s) = (/^>(\S+)/))
      	{
      		
      		if ($seq ne "" && $id ne "") 
			{ 
				$h_seq{$id}=$seq;
				$seq="";
			}
			$id=$s;
      	}
      	else
      	{
			$seq.=$_;
      	}
    }
	$h_seq{$id}=$seq;
	close (IN);
	#Pour chaque séquence SNP du fichier
	open (IN,$vcf_file);
	my $previous_chromosome="";
	my $current_seq="";
	my $current_len=0;
	while ( my $line = <IN>) 
	{
		if ($line !~ /^##/)
		{
			chomp $line ;
			my @a_columns = split (/\t/, $line);
			
			
			my $chromosome = $a_columns[0];
			my $position = $a_columns[1];
			my $ref_allele = $a_columns[3];
				
			my @alt_alleles = split (/,/, $a_columns[4]);
			##INDEL
			if ( $a_columns[4] =~ /\w{2,}/ || $ref_allele=~ /\w{2,}/ )
			{
				next;
			}
			if ($chromosome ne $previous_chromosome)
			{
				$current_seq=$h_seq{$chromosome};
				$current_len=length($current_seq);
			}
			my $seq_desc = "ALLELE:".join("/",@alt_alleles)." SNP:".$position;
			my $position_in_string=$position-1;

			my $five_prime_offset = $nb_base_flanking-1; # -1 -> don't include snp
			my $five_prime_start = $position_in_string-$five_prime_offset;
			my $three_prime_offset = $nb_base_flanking;			
			my $three_prime_start = $position_in_string+1;
			if ($five_prime_start < 0) 
			{
				$five_prime_start=0;
				$five_prime_offset=$position_in_string-1;
			}
			if ($three_prime_start+$three_prime_offset > $current_len-1)
			{
				$three_prime_offset=$current_len-1-$position_in_string;
				
			}
			my $sequence = "";
			if ($five_prime_offset>0) 
			{
				$sequence .=substr($current_seq,$five_prime_start,$five_prime_offset);
			}
			$sequence .=$ref_allele;
			if ($three_prime_offset>0) 
			{
				$sequence .=substr($current_seq,$three_prime_start,$three_prime_offset);
			}
			
			my $snp = Bio::Seq->new( 
								-id   => $chromosome."_".$position,
								-seq  => $sequence,
								-desc => $seq_desc );
								
			$out_seq->write_seq($snp);	
			
			
		}
	}

}


=head2 function get_initial_desc

 Title        : get_initial_desc
 Usage        : $description = get_initial_desc( $snp_header )
 Prerequisite : none
 Function     : returns the description of the SNP as it was in the
                format not usable by the alignment software.
 Returns      : string
 Args         : $snp_header     string description of a sequence in
                                fasta format usable by blast
 Globals      : none

=cut

sub get_initial_desc
{
	my $snp_desc = $_[0] ;
	
	$snp_desc =~ /^(.*)ALLELE:[^\s]+\sSNP:\d+$/i ;

	#Si le header ne permet pas de récupérer la description initiale
	if(!defined $1)
	{
		die "[ERROR] Cannot return the initial description from : ".$_[0].".\n" ;
	}
	
	return $1 ;
}


=head2 function get_possible_values_snp

 Title        : get_possible_values_snp
 Usage        : $values = get_possible_values_snp( $snp_header )
 Prerequisite : none
 Function     : returns the possible nucleic acids list for the SNP.
 Returns      : list
 Args         : $snp_header     string Id and description of a sequence
                                in fasta format usable by blast
 Globals      : none

=cut

sub get_possible_values_snp
{
	my $snp_header = $_[0] ;
	my @possible_nucleic_acids ;
	
	$snp_header =~ /ALLELE:([^\s]+)\sSNP/i ;
	
	#Si les acides nucléiques possible ne sont pas indiqués
	if(!defined $1)
	{
		die "[ERROR] The possible values for the snp are not indicated in $snp_header.\n" ;
	}
	
	#Créer une liste à partir de la chaine de caractères
	@possible_nucleic_acids = split('/', $1) ;
	
	return @possible_nucleic_acids ;
}


=head2 function get_snp_position

 Title        : get_snp_position
 Usage        : $position = get_snp_position( $snp_header )
 Prerequisite : none
 Function     : returns the SNP's position on the first sequence of the
                file.
 Returns      : int
 Args         : $snp_header     string Id and description of a sequence
                                in fasta format usable by blast
 Globals      : none

=cut

sub get_snp_position
{
	my $snp_header = $_[0] ;
	
	$snp_header =~ /SNP:([^\s]+)\s*$/i ;
	
	#Si la position du SNP n'est pas indiqué
	if(!defined $1)
	{
		die "[ERROR] SNP's position is not indicated in $snp_header.\n" ;
	}
	
	return $1 ;
}


=head2 function is_fasta_blatable_file

 Title        : is_fasta_blatable_file
 Usage        : $structure_is_correct = is_fasta_blatable_file(
                $snp_file, $snp_file_formated, $file_format )
 Prerequisite : none
 Function     : returns 1 if the file is in fasta blastable format. In
                this format sequences correspond to one of the alleles,
                and description indicates the SNP's position on the
                sequence and possible values.
 Returns      : binary
 Args         : $snp_file        string File of polymorphism in Fasta
                                 format
 Globals      : none

=cut

sub is_fasta_blast_usable
{
	my $snp_file = $_[0] ;
	my $structure_is_correct = 1 ;
	my $precedent_line = '' ;
	
	#Ouvrir le fichier
	open(SNP_FILE, $snp_file) or die "[ERROR] Cannot open file $snp_file.\n" ;

	#Pour chaque ligne du fichier tant que la structure est correcte
	while ( defined(my $line = <SNP_FILE>) && $structure_is_correct )
	{
		#Si la ligne actuelle correspond à une ligne de séquence
		if( $line =~ /^[ATGCN-]+\s*$/i )
		{
			$precedent_line = 'sequence' ;
		}
		#Si on a affaire à une ligne de description, qu'elle n'est pas 
		#précédé d'une autre ligne de description, et qu'elle s'achève
		#par l'indication des valeurs possible et de la position du SNP
		elsif( ($line =~ /^>.+/) &&	$precedent_line ne 'description'
				&& ($line =~ /ALLELE:[ATGC](\/[ATGC])+ SNP:\d+s*$/i) )
		{		
			$precedent_line = 'description' ;
		}
		#Dans tous les autre cas la structure n'est pas correcte
		else
		{
			$structure_is_correct = 0 ;
		}
	}
	
	close SNP_FILE ;
	
	return $structure_is_correct ;
}


=head2 function is_fasta_polymorphism_file

 Title        : is_fasta_polymorphism_file
 Usage        : $structure_is_correct = is_fasta_polymorphism_file( 
                $snp_file, $snp_file_formated, $file_format )
 Prerequisite : none
 Function     : returns 1 if the file is a file of polymorphism in fasta
                format. In this format, SNP are included in sequences
                with this syntax [nucleic_ac/nucleic_ac].
 Returns      : binary
 Args         : $snp_file         string File of polymorphism in Fasta
                                  format 
 Globals      : none

=cut

sub is_fasta_polymorphism_file
{
	my $snp_file             = $_[0] ;
	my $seq                  = 'A[A/T]A' ;
	my $precedent_line       = '' ;
	my $structure_is_correct = 1 ;

	#Créer l'expression régulière permetant de vérifier la structure
	#d'une séquence du fichier 
	my $snp_structure = '\[[ACGTN](\/[ACGTN])+\]' ;
	my $sequence_structure = '[ATGCUNRYMKWSBDHV\-\s]' ;
	my $sequence = '^'.$sequence_structure.'*'.$snp_structure.$sequence_structure.'*\s*$' ;

	#Ouvrir le fichier
	open(SNP_FILE, $snp_file) or die "[ERROR] Cannot open file $snp_file. ".$! ;

	#Pour chaque ligne du fichier tant que la structure est correcte
	while ( defined(my $line = <SNP_FILE>) && $structure_is_correct )
	{
		#Si la ligne actuelle correspond à une ligne de séquence
		if( $line =~ /^[ATGCUNRYMKWSBDHV\s\-\[\]\/]+\n$/i )
		{
			#Si le document commence par une séquence sans en-tête
			if( $precedent_line eq '' )
			{
				$structure_is_correct = 0 ;
			}

			#Concaténer la séquence
			$seq .= $line ;
			$precedent_line = 'sequence' ;
		}
		#Si la ligne actuelle correspond à une ligne de description
		elsif( $line =~ /^>/ )
		{
			#Si la ligne précédente était déjà une ligne de description
			if( $precedent_line eq 'description' )
			{
				$structure_is_correct = 0 ;
			}

			#Retirer les sauts de ligne de la séquence snp précédente
			$seq =~ s/\n//g ;

			#Si la séquence snp précédente ne contenait pas de snp
			if( !($seq =~ /$sequence/i) )
			{
				$structure_is_correct = 0 ;
			}

			
			$seq = '' ;
			$precedent_line = 'description' ;
		}
		#Si la ligne actuelle n'a pas une syntaxe correcte
		else
		{
			$structure_is_correct = 0 ;
		}
	}
	#Si la dernière séquence snp ne contenait pas de snp
	if( !($seq =~ /$sequence/i) )
	{
		$structure_is_correct = 0 ;
	}
	
	close SNP_FILE ;
	
	return $structure_is_correct ;
}


=head2 function is_tab_polymorphism_file

 Title        : is_tab_polymorphism_file
 Usage        : $structure_is_correct = is_tab_polymorphism_file(
                $snp_file, $snp_file_formated, $file_format )
 Prerequisite : none
 Function     : returns 1 if the file is a file of polymorphism in the
                tabular format. Format :
				id	snp_position	left_flanking_seq	nuc_ac/nuc_ac 
				right_flanking_seq
 Returns      : binary
 Args         : $snp_file         string File of polymorphism in tabular
                                  format
 Globals      : none

=cut

sub is_tab_polymorphism_file
{
	my $snp_file = $_[0] ;
	my $structure_is_correct = 1 ;
	
	#Créer l'expression régulière permetant de vérifier la structure
	#d'une ligne du fichier : 
	#identifiant | positionSnp | lanquanteGauche | valeurPossibles | flanquanteDroite
	#Les ' | ' représentent des tabulations
	my $position_structure = '\d+' ;
	my $variant_structure = '[ATGCUNRYMKWSBDHV-]+(\/[ATGCUNRYMKWSBDHV-]+)+' ;
	my $flanking_sequences_structure = '[ATGCUNRYMKWSBDHV-]*' ;
	my $correct_structure = '^[^\t]+\t'.$position_structure.'\t'
				.$flanking_sequences_structure.'\t'.$variant_structure
				.'\t'.$flanking_sequences_structure.'\s*$' ;

	#Ouvrir le fichier
	open(SNP_FILE, $snp_file) or die "[ERROR] Cannot open file $snp_file. ".$! ;

	#Pour chaque ligne du fichier tant que la structure est correcte
	while ( defined(my $line = <SNP_FILE>) && $structure_is_correct )
	{
		#Si la structure de la ligne n'est pas correcte
		if( !( $line =~ /$correct_structure/i ) )
		{
			$structure_is_correct = 0 ;
		}
	}
	
	close SNP_FILE ;
	
	return $structure_is_correct ;
}


=head2 procedure tab_to_blastable_snp_file

 Title        : tab_to_blastable_snp_file
 Usage        : tab_to_blastable_snp_file( $snp_file, $snp_file_formated,
                $file_format )
 Prerequisite : packages Bio::SeqIO and Bio::Seq
 Function     : creates from a tabular file a file of polymorphism
                usable by blast. In this format sequences correspond to
                one of the alleles, and description indicates the SNP's
                position on the sequence and possible values.
 Returns      : none
 Args         : $snp_file            string File of polymorphism in
                                     tabular format :
								     id snp_position left_flanking_seq
								     nuc_ac/nuc_ac right_flanking_seq
				
				$snp_file_formated   string Reformatted file of
                                     polymorphism
				
                $file_format         string File format generated
                                     (example : 'Fasta')
 Globals      : none

=cut

sub tab_to_blast_usable
{
	my ($snp_file, $snp_file_formated, $file_format) = @_ ;

	#Créer un fichier Fasta
	my $out_seq = Bio::SeqIO->new(
								-file => ">".$snp_file_formated, 
								'-format' => $file_format
								);

	open(SNP_FILE, $snp_file) or die "[ERROR] Cannot open file $snp_file.\n" ;

	#Pour chaque ligne
	foreach my $line (<SNP_FILE>)
	{								
		#Si on a pas affaire à une ligne vide
		if(!($line =~ /^\s+$/))
		{
			#Récupérer tous les champs
			my @champs = split(/\t/, $line) ;
			
			#Créer l'en-tête de la séquence
			my ($id,$description) = $champs[0] =~ /(\S+)\s*(.*)/;
						
			#pas d'indel : ils ne sont pas encore gérés
			if ( $champs[3] =~ /^[ACGTN](\/[ACGTN])+?$/i )
			{
				
				#Ajouter les acides nucléiques possible pour le SNP
				$description .= " ALLELE:".$champs[3] ;
			
				#Ajouter la localisation du SNP sur la séquence
				$champs[2] =~ s/[\s-]//g ;
				$description .= " SNP:".( length( $champs[2] ) + 1 ) ;

				#Récupérer le premier des acides nucléiques possibles	
				#pour le SNP
				my $val_snp = substr($champs[3], 0, 1) ;
			
				#Créer la séquence SNP complète (région gauche + SNP + 
				#région droite)
				 $champs[4]=~ s/[\s-]//g ;
				my $seq = join ($val_snp, $champs[2], $champs[4]) ;
				#$seq =~ s/[\s-]//g ;

				#Ecrire la séquence reformaté dans le nouveau fichier
				my $snp = Bio::Seq->new( -id   => $id,
									 -desc => $description,
									 -seq  => $seq);
			
				$out_seq->write_seq($snp);
			}
			else
			{
				warn "Snp $id discard from analysis, format '$champs[3]' isn't handle [tab_to_blast_usable function]\n";
			}
		}
	}
	
	close SNP_FILE ;
}


1;
