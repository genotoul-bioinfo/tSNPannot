# $Id$

=pod
=head1 NAME

snpAnnotateChecker - check validity of snpAnnotate's parameters

=head1 DESCRIPTION

 This package is used by snpAnnotate to check validity of its
 parameters. It contains functions to check : 
	the dependencies,
	the requiered parameters,
	the existence of each species on Ensembl.

=cut

use strict ;
use ensemblUtilities ;
use utilities ;



=head2 procedure check_dependencies

 Title        : check_dependencies
 Usage        : check_dependencies( $blast_program )
 Prerequisite : Unix operating system and package ConfigManager
 Function     : Verify that the specified program is installed and is in
                list of the usable programs for the application
 Returns      : none
 Args         : $blast_program      string Program used to search 
                                    similar proteins
 Globals      : none

=cut

sub check_dependencies
{
	my $blast_program = $_[0] ;
	
	#Récupérer la liste des programmes de recherche de similarité valides
	my $config = get_instance ConfigManager () ;
	my $available_soft = $config->get_param("blast_programs") ;
	
	#transformer la liste des programme en hachage
	my %available ;
	@available{@{$available_soft}} = @{$available_soft} ;

	#Si le programme spécifié ne fait pas partie des programmes valides
	if( !defined($available{$blast_program}) )
	{
		die "[ERROR] The blast program must be ".join(" or ", @{$available_soft}).".\n" ;
	}
	
	#Vérifier que le programme d'alignement demandé existe et est exécutable
	if( !dependency_is_installed($blast_program) )
	{
		die "[ERROR] $blast_program does not seem to be installed on your computer. "
			."This is a program required to execute $0.\n" ;
	}
}


=head2 procedure check_parameters

 Title        : check_parameters
 Usage        : check_parameters( $species, $snp_file, $output_file,
                $output_format, $err_param, $help )
 Prerequisite : none
 Function     : Verify that mandatory parameters are shown, check value
                of outtype and call for help if it is requested
 Returns      : none
 Args         : $species                    string 'species' parameter
                                            of the program
                
                $snp_file                   string 'snpfile' parameter
                                            of the program
                
                $output_file                string 'outfile' parameter
                                            of the program
                
                $output_format              string 'outtype' parameter
                                            of the program
                
                $err_param                  int Indicates if type of all
                                            program parameters is
                                            correct
                
                $help                       int 'h' or 'help' parameter
                                            of the program
 Globals      : none

=cut

sub check_parameters
{
	my ( $species, $snp_file, $output_file, $output_format, $err_param,
	     $help, $reference_file ) = @_ ;
	my $error_msg = '' ;

	#Si l'aide est demandée, afficher l'usage et ne pas exécuter 
	#le programme
	if($help)
	{
		usage() ;
	}

	#Si le paramètre species est manquant ajouter une indication sur
	#le message d'erreur	
	if(!@{$species})
	{
		 $error_msg .= "'--species'\tensembl species is required.\n" ;
	}

	#Si le paramètre snpfile est manquant ajouter une indication sur
	#le message d'erreur
	if(!$snp_file)
	{
		$error_msg .= "'--snpfile'\tsnpfile species is required\n" ;
	}
	#Si le fichier spécifié n'existe pas
	elsif(! -e $snp_file)
	{
		$error_msg .= "The file ".$snp_file." does not exist.\n" ;
	}
	#Si l'élément spécifié n'existe pas un fichier
	elsif(! -f $snp_file)
	{
		$error_msg .= $snp_file." isn't a file.\n" ;
	}
	
	if( $snp_file =~ /.vcf$/ && (! $reference_file || ! -e $reference_file) )
	{
		$error_msg .= "If input file is a named '.vcf', option --reffile is mandatory and must exist.\n" ;
	}
	
	#Si le paramètre outfile est manquant ajouter une indication sur
	#le message d'erreur
	if(!$output_file)
	{
		$error_msg .= "'--outfile'\tis mandatory.\n" ;
	}

	#Si le paramètre outype n'indique pas un format valide ajouter une
	#indication sur le message d'erreur
	if($output_format ne 'txt' && $output_format ne 'xml')
	{
		$error_msg .= "'--".$output_format."'\tis not a correct output "
					."format for this program.\n" ;
	}
	
	#Si des paramètres sont manquants ou dans un format invalide stopper
	#l'exécution du programme et afficher le message d'erreur
	if($error_msg ne '' || $err_param)
	{
		die $error_msg."[ERROR] See help for more information ($0 -help).\n" ;
	}
}


=head2 procedure check_species

 Title        : check_species
 Usage        : check_species( $species_ref )
 Prerequisite : packages Core of the Ensembl API and ensemblUtilities
 Function     : checks if all reference species used for the annotation
                are in Ensembl database and in Ensembl variation
                database. If one species is absent from Ensembl : the
                program stops. For each species without variation data :
                a warning is triggered.
 Returns      : none
 Args         : $species_ref          array reference List of species
 Globals      : none
 
=cut

sub check_species
{
	my ($species_ref) = @_ ;
	my $error_msg = '' ;

	#Pour chaque espèce
	foreach my $current_species ( @{$species_ref} )
	{
		#Si l'espèce n'existe pas sur Ensembl (et ensembl genome)
		if( !is_in_ensembl_core($current_species) )
		{
			$error_msg .= $current_species." does not exist in Ensembl or Ensemblgenomes banks.\n" ;
		}
		#Si l'espèce existe sur Ensembl mais ne possède pas de données de variation
		elsif( !is_in_ensembl_variation($current_species) )
		{
			warn "WARNING : Ensembl or Ensemblgenomes doesn't store variation data for ".$current_species."." ;
		}
	}
	
	#Si une des espèce ne possède pas de données sur Ensembl
	if($error_msg ne '')
	{
		die "[ERROR]\n".$error_msg ;
	}
}


1;
