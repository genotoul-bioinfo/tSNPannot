# $Id$

=pod
=head1 NAME

utilities - Frequently used functions in programs

=head1 DESCRIPTION

 This package provides functions frequently used in programs.

=cut


=head2 function dependency_is_installed

 Title        : dependency is installed
 Usage        : $binary = dependency_is_installed( $software )
 Prerequisite : unix system
 Function     : Returns 1 if the program is installed on computer.
 Returns      : boolean
 Args         : $software               string Program name
 Globals      : none

=cut

sub dependency_is_installed
{
	my ( $software ) = @_ ;
	
	#Appel système pour vérifier que formatdb existe et est exécutable
	`which $software` ;

	#Si la valeur de retour de l'appel système n'est pas zéro c'est que
	#le programme n'est pas présent ou pas exécutable
	return ( $? == 0 )
}

1 ;
