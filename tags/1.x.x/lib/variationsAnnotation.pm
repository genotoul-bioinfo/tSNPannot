# $Id$

=pod
=head1 NAME

variationsAnnotation - Provides functions to annotate consequences of a
                       variation

=head1 DESCRIPTION

 This package provides annotation functions for consequences of a 
 variation.
 You can obtain :
    - if exist a reference variation on the specified position of the
      genome, its known consequences, 
    - the consequences of the variation on the transcript of the
      reference species,
    - the new amino acid in protein after nucleic acid replacement  

=cut

use strict ;
use Bio::EnsEMBL::Registry ;
use coordinates ;



=head2 function get_all_var_on

 Title        : get_all_var_on
 Usage        : @variant = get_all_var_on( $position_on_genome,
                $variation_feature_adaptor[, $variant_type] )
 Prerequisite : package Try::Tiny and Variation and Core of the Ensembl
                API
 Function     : returns the list of existing variations at the specified
                location on the genome. The type of variation can be
                specified.
 Returns      : list
 Args         : $position_on_genome            EnsEMBL::Feature
                                               A position on the genome
                
                $variation_feature_adaptor     EnsEMBL::Variation::
                                               DBSQL::VariationAdaptor
                                               adapter for variations on
                                               a given genome
				
				$variant_type                  string Type of desired
                                               variation (example : SNP)
 Globals      : none

=cut

sub get_all_var_on
{
	my ($position_on_genome, $variation_feature_adaptor, $variant_type) = @_ ;
	my $variants_ref ;
	
	#Initialiser la référence sur un tableau vide
	my @empty_tab = () ;
    $variants_ref = \@empty_tab ;

	
	#Récupérer un segment de génome correspondant à la position indiqué
	#sur le génome de référence
	my $slice = $position_on_genome->slice->sub_Slice(
											$position_on_genome->start(), 
											$position_on_genome->end(), 
											$position_on_genome->strand()) ;

	
	#Récupérer toutes les variations existant dans le génome de référence
	#pour la position indiqué
	my $variation_features = $variation_feature_adaptor->fetch_all_by_Slice($slice) ;
	push(@{$variation_features}, @{$variation_feature_adaptor->fetch_all_somatic_by_Slice($slice)}) ;

	#Si un filtre est définit
	if(defined $variant_type)
	{
		#Pour chaque variation existant sur cette position dans le génome
		my $i = 0 ;
		foreach my $variation (@{$variation_features})
		{
			#Si la variation est de la classe souhaité
			if( $variation->var_class() eq $variant_type )
			{
				$$variants_ref[$i] = $variation ;
				$i++ ;
			}
		}
	}
	#Si aucun filtre n'est définit
	else
	{
		$variants_ref = $variation_features ;
	}
	
	return $variants_ref ;
}


=head2 function get_allele_consequences

 Title        : get_allele_consequences
 Usage        : \%allele_consequences = get_allele_consequences(
                $ref_nucleic_acid, $allele, $position_on_genome,
                $variation_feature_adaptor )
 Prerequisite : package Variation and Core of the Ensembl API
 Function     : returns a list of consequences for each transcript
                affected by the replacement of a nucleic acid at a given
                place in the genome.
 Returns      : hash reference
                Structure of the hash :
                {'ENSDART00000058403'}{'consequence_1'} 
                                                  => 'STOP_GAINED'
                {'ENSDART00000058403'}{'consequence_2'} 
                                                  => 'UPSTREAM'
                {'ENSDART00000055857'}{'consequence_1'} 
                                                  => 'SYNONYMOUS_CODING'
 Args         : $ref_nucleic_acid          char Reference nucleic acid
                                           where indicated on the Genome
                
                $allele                    char Nucleic acid that
                                           replaces the existing one at
                                           the given position
                
                $position_on_genome        EnsEMBL::Feature Position on
                                           genome that is varied
                
                $variation_feature_adaptor EnsEMBL::Variation::
                                           DBSQL::VariationAdaptor
                                           adapter for variations on a
                                           given genome
 Globals      : none

=cut

sub get_allele_consequences
{
	my ($ref_nucleic_acid, $allele, $position_on_genome, $variation_feature_adaptor) = @_ ;
	my %allele_consequences ;

	
	#Créer l'objet VariationFeature correspondant au SNP
	my $new_vf = Bio::EnsEMBL::Variation::VariationFeature->new(
	  -start          => $position_on_genome->start(),
	  -end            => $position_on_genome->end(),
	  -slice          => $position_on_genome->slice(),		# The variation must be attached to a slice
	  -allele_string  => $ref_nucleic_acid.'/'.$allele,		# The first allele should be the reference allele
	  -strand         => $position_on_genome->strand(),
	  -map_weight     => 1,									# Is the number of times this features variation was mapped to he genome
	  -adaptor        => $variation_feature_adaptor,
	  -variation_name => 'newSNP'
	);

	#Récupérer les conséquences sur les transcrits de la variation
	my $transcripts_consequence = $new_vf->get_all_TranscriptVariations();

	#Pour chaque transcrit affectée par la variation
	foreach my $transcripts_variation ( @{$transcripts_consequence} ) 
	{								
		#Pour chaque type de conséquence
		my $i = 1 ;
		foreach my $consequence ( @{ $transcripts_variation->consequence_type} ) 
		{
			#Stocker la consequence sur le transcrit	
			$allele_consequences{$transcripts_variation->transcript->stable_id}{'consequence_'.$i} = $consequence ;
			$i++ ;
		}
	}

	return \%allele_consequences ;
}


=head2 function get_existing_variation

 Title        : get_existing_variation
 Usage        : @variant = get_existing_variation( $position_on_genome,
                $species, $variant_type, $registry )
 Prerequisite : package Try::Tiny and Variation and Core of the Ensembl
                API
 Function     : returns the list of existing variations at the specified
                location on the genome. The type of variation must be
                specified.
 Returns      : list
 Args         : $position_on_genome        EnsEMBL::Feature
                                           A position on the genome
                
                $species                   string The reference species
                
                $variant_type              string Type of desired
                                           variation (example : SNP)
                
                $registry                  EnsEMBL::Registry The
                                           connection to the database
                                           Ensembl
 Globals      : none

=cut

sub get_existing_variation
{
	my ($position_on_genome, $species, $variant_type, $registry) = @_ ;
	my @variant_desc ;
	
	#Créer un adaptateur sur les variations du génome de l'espèce de référence
	my $variation_feature_adaptor = $registry->get_adaptor($species, 'variation', 'variationfeature') 
		or return @variant_desc ;
	
	#Récupérer les variations existant à la position
	my $variants_ref = get_all_var_on($position_on_genome, $variation_feature_adaptor, $variant_type) ;

	#Pour chaque variation existant sur cette position dans le génome
	my $i = 0 ;
	foreach my $variation (@{$variants_ref})
	{
		#Stocker le nom du variant dans la liste de SNP
		$variant_desc[$i]{'name'}   = $variation->variation_name ;
			
		#Récupérer les allèles du variant (ce sont toujours les 
		#variant sur le brin forward qui sont retournés)
		$variant_desc[$i]{'allele'} = $variation->allele_string ;

		#Si le brin qui nous intéresse est le brin reverse
		if( $position_on_genome->strand() == -1)
		{
			#Transformer les acides nucléiques possibles en leurs 
			#complémentaires
			$variant_desc[$i]{'allele'} =~ tr/ATGCatgc/TACGtacg/ ;
		}

		$i++ ;
	}
	
	return @variant_desc ;
}


=head2 function get_known_allele_consequences

 Title        : get_known_allele_consequences
 Usage        : \%allele_consequences = get_known_allele_consequences(
                $allele, $position_on_genome, 
                $variation_feature_adaptor )
 Prerequisite : package Variation and Core of the Ensembl API
 Function     : returns a list of consequences for each transcript
                affected by the known replacement of a nucleic acid at a
                given place in the genome. This list is empty if the
                replacement is not known.
 Returns      : hash reference
                Structure of the hash :
                {'ENSDART00000058403'}{'consequence_1'} 
                                                  => 'STOP_GAINED'
                {'ENSDART00000058403'}{'consequence_2'} 
                                                  => 'UPSTREAM'
                {'ENSDART00000055857'}{'consequence_1'} 
                                                  => 'SYNONYMOUS_CODING'
 Args         : $allele                    char Nucleic acid that
                                           replaces the existing one at
                                           the given position
                
                $position_on_genome        EnsEMBL::Feature Position on
                                           genome that is varied
                
                $variation_feature_adaptor EnsEMBL::Variation::
                                           DBSQL::VariationAdaptor
                                           adapter for variations on a
                                           given genome
 Globals      : none

=cut

sub get_known_allele_consequences
{
	my ($allele, $position_on_genome, $variation_feature_adaptor) = @_ ;
	my %allele_consequences ;
	my $is_known_allele = 0 ;
	
	#Considérer l'allèle sur le brin forward car les variations référencées
	#dans Ensembl sont considérées sur ce brin 
	if( $position_on_genome->strand() == -1 )
	{
		$allele =~ tr/ATGCatgc/TACGtacg/ ;
	}
	
	#Récupérer les variations existant à la position indiqué
	my $variations_feat_ref = get_all_var_on($position_on_genome, $variation_feature_adaptor, 'SNP') ;

	#Pour chaque variations existantes à la position déterminé et tant
	#qu'aucun allèle ne correspond à celui dont on veut les conséquences
	for(my $i = 0 ; $i < @{$variations_feat_ref} && !$is_known_allele ; $i++)
	{
		#Récupérer tous les allèles de la variation
		my @alleles = split('/', $$variations_feat_ref[$i]->allele_string()) ;

		#Pour chaque allèle, tant qu'aucun d'entre eux ne correspond
		#à celui dont on veut les conséquences
		for(my $j = 0 ; $j < @alleles && !$is_known_allele ; $j++)
		{
			#Si l'allèle correspond à celui dont on cherche la conséquence
			if($allele eq $alleles[$j])
			{
				#Indiquer qu'un allèle identique a été trouvé
				$is_known_allele = 1 ;
				
				#Récupérer les conséquences sur les transcrits de cet allèle
				my $transcripts_consequence = $$variations_feat_ref[$i]->get_all_TranscriptVariations();

				#Pour chaque transcrit affectée par la variation
				foreach my $transcripts_variation ( @{$transcripts_consequence} ) 
				{								
					#Pour chaque type de conséquence
					my $k = 1 ;
					foreach my $consequence ( @{ $transcripts_variation->consequence_type} ) 
					{
						#Stocker la conséquence sur le transcrit	
						$allele_consequences{$transcripts_variation->transcript->stable_id}{'consequence_'.$k} = $consequence ;
						$k++ ;
					}
				}
			}
		}
	}

	return \%allele_consequences ;
}


=head2 function variant_consequences_on_ref_species

 Title        : variant_consequences_on_ref_species
 Usage        : %snp_consequences = variant_consequences_on_ref_species(
                $position_on_genome, $possible_values_snp_ref, $species,
                $registry )
 Prerequisite : package Try::Tiny and Variation and Core of the Ensembl
                API
 Function     : returns for each variation the consequences for each
                affected transcript.
 Returns      : hash
                Structure of the hash :
                {'G/T'}{$trancript_id}{'consequence_1'} 
                                                  => 'SYNONYMOUS_CODING'
                {'G/T'}{$trancript_id}{'consequence_1'} 
                                                  => 'SYNONYMOUS_CODING'
				{'G/A'}{$trancript_id}{'consequence_1'} 
                                                  => 'SYNONYMOUS_CODING'
                {'G/G'}{'null'} => 'NO_CHANGE'
 Args         : $position_on_genome            EnsEMBL::Feature Position
                                               on genome that is varied
                
                $ref_possible_values_snp_ref   array reference List of
                                               nucleic acids whose 
                                               consequences are to be
                                               studied
                
                $species                       string The reference
                                               species
                
                $registry                      EnsEMBL::Registry The
                                               connection to the
                                               database Ensembl
 Globals      : none

=cut

sub variant_consequences_on_ref_species
{
	my ($position_on_genome, $possible_values_snp_ref, $species, $registry) = @_ ;
	my %snp_consequences ;
		
	#Récupérer un segment de génome correspondant à la position indiqué
	#sur le génome de référence
	my $slice = $position_on_genome->slice->sub_Slice(
											$position_on_genome->start(), 
											$position_on_genome->end(), 
											$position_on_genome->strand()) ;
	
	#Créer un adaptateur pour les variations sur le génome de référence
	my $variation_feature_adaptor = $registry->get_adaptor($species, 'variation', 'variationfeature')
			or die "[ERROR] Ensembl doesn't store variation data for ".$species ;
	
	#Récupérer l'acide nucléique de référence sur cette position dans le
	#génome
	my $ref_nucleic_acid = $slice->seq() ;

	#Pour chaque acide nucléique possible pour le SNP sur la séquence SNP
	foreach my $an_allele (@{$possible_values_snp_ref})
	{
			#Si le nucléotide du SNP est différent de celui de référence
			#sur le génome de référence
			if( uc($an_allele) ne uc($ref_nucleic_acid) )
			{	
				#Stocker les conséquences de l'allèle dans le hachage contenant 
				#les conséquences de chaque allèle du SNP
					#Récupérer les conséquences de l'allèle s'il est déjà caractérisé (il existe sur un SNP de référence)
					$snp_consequences{$ref_nucleic_acid.'/'.$an_allele} = get_known_allele_consequences($an_allele, $position_on_genome, $variation_feature_adaptor) ;

					#Si l'allèle n'existe pas sur le génome de référence
					if( !%{$snp_consequences{$ref_nucleic_acid.'/'.$an_allele}} )
					{
						#Calculer l'effet de cet allèle
						$snp_consequences{$ref_nucleic_acid.'/'.$an_allele} = get_allele_consequences($ref_nucleic_acid, $an_allele, $position_on_genome, $variation_feature_adaptor) ;
					}
			}
			#Si le nucléotide du SNP est identique à celui sur le génome
			#de référence (pas de vraiation)
	}

	return \%snp_consequences ;
}


=head2 function variant_consequences_on_transcript

 Title        : variant_consequences_on_transcript
 Usage        : $new_aa = variant_consequences_on_transcript( 
                $sequence, $position_on_sequence, $position_on_codon, 
                $new_allele )
 Prerequisite : none
 Function     : determines the consequence of a nucleic acid replacement
                on a transcript sequence. The function returns the three
                letter code of the new amino acid or * for a stop codon.
 Returns      : string
 Args         : $sequence                   string Nucleic acid sequence
                                            where the replacement is
                                            done
                
                $position_on_sequence       int Position on sequence
                                            where the replaced nucleic
                                            acid is
                
                $position_on_codon          int Position on its codon
                                            where the replaced nucleic
                                            acid is
                
                $new_allele                 char New value for the
                                            nucleic acid
 Globals      : none

=cut

sub variant_consequences_on_transcript
{
	my ($sequence, $position_on_sequence, $position_on_codon, $new_allele) = @_ ;
	
	#Si la séquence ne contient pas le codon en entier 
	if(
		($position_on_sequence == 1 && $position_on_codon != 1) ||
		($position_on_sequence == 2 && $position_on_codon == 3) ||
		($position_on_sequence == (length $sequence) && $position_on_codon != 3) ||
		($position_on_sequence == ((length $sequence) - 1) && $position_on_codon == 1)
	)
	{
		die "[ERROR] The sequence does not contain all the elements of the codon. "
			."The consequences of the new allele can not be determined.\n" ;
	}
	
	#Calculer la position du premier acide nucléique du codon
	my $position_begin_of_codon = $position_on_sequence - $position_on_codon + 1 ;
	
	#Codon avant mutation
	my $ref_codon = substr( $sequence, $position_begin_of_codon - 1, 3) ;
	
	#Codon avec le nouvel allèle
	my @codon = split(//, $ref_codon) ;
	$codon[$position_on_codon - 1] = $new_allele ;
	my $new_codon = join('', @codon) ;
	
	#Nouvel acide aminé
	my $new_aa = codon_to_aa($new_codon) ;
	
	return $new_aa ;
}

=head2 function get_base_on_ref

 Title        : get_base_on_ref
 Usage        : @variant = get_base_on_ref( $position_on_genome,
                $species, $registry )
 Prerequisite : package Try::Tiny and Core of the Ensembl API
 Function     : returns the reference base at the specified
                location on the genome. 
 Returns      : character
 Args         : $position_on_genome        EnsEMBL::Feature
                                           A position on the genome
                
                $species                   string The reference species
                
                $registry                  EnsEMBL::Registry The
                                           connection to the database
                                           Ensembl
 Globals      : none

=cut

sub get_base_on_ref
{
	my ($position_on_genome, $species, $registry) = @_ ;
		
	#Récupérer un segment de génome correspondant à la position indiqué
	#sur le génome de référence
	my $slice = $position_on_genome->slice->sub_Slice(
											$position_on_genome->start(), 
											$position_on_genome->end(), 
											$position_on_genome->strand()) ;
											
	#Récupérer la base existante à la position
	my @base = $slice->seq();
	my $car = $base[0];
	return $car ;
}


1 ;
