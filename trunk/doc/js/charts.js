// Radialize the colors
Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
    return {
        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
        stops: [
            [0, color],
            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')]
        ]
    };
});


/**
 * Return stacked bar chart settings.
 */
var stackedBarChart = function(title, yaxis_title, data) {
	var formated_data_hash = {} ;
	var categories = new Array() ;
	for( var idx in data ) {
		categories.push( data[idx]["name"] );
		series = Object.keys(data[idx]["data"]).sort() ;
		for( var series_idx in series ) {
			if( !formated_data_hash.hasOwnProperty(series[series_idx]) ){
				formated_data_hash[series[series_idx]] = new Array();
			}
			formated_data_hash[series[series_idx]].push( data[idx]["data"][series[series_idx]] );
		}
	}
	var formated_data = [] ;
	Object.keys(formated_data_hash).forEach(function (key) {
		formated_data.push({
			"name" : key,
			"data" : formated_data_hash[key]
		})
	});

    return ({
    	chart: {
	        type: 'bar'
	    },
	    title: {
	        text: title
	    },
	    xAxis: {
	        categories: categories
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: yaxis_title
	        }
	    },
	    legend: {
	        reversed: true
	    },
	    plotOptions: {
	        series: {
	            stacking: 'normal'
	        }
	    },
	    series: formated_data
    })
}


/**
 * Return pie chart settings.
 */
var pieChart = function(title, data, selected) {
    var formated_data = new Array();	
    for( var category in data ){
        if( !(category in selected) ) { 
            formated_data.push( new Array(category, data[category]) );
        } else {
            var new_data = {
                            name: category,
                            y: data[category],
                            sliced: true,
                            selected: true
            }
            formated_data.push( new_data );
        }
    }
    return({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: title
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'share',
            data: formated_data
        }]
    });
}


/**
 * Return scatter chart settings.
 */
var scatterChart = function(title, sub_title, xaxis_title, yaxis_title, series) {
	return({
		chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        title: {
            text: title
        },
        subtitle: {
            text: sub_title
        },
        xAxis: {
            title: {
                enabled: true,
                text: xaxis_title
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true
        },
        yAxis: {
            title: {
                text: yaxis_title
            }
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 100,
            y: 70,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
            borderWidth: 1
        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x} : {point.y}'
                }
            }
        },
        series: series
	})
}