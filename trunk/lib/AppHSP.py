# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


import NCBIXML
from Record import HSP

class AppHSP(HSP):
    """
    @summary : An AppHSP is a Bio.Blast.Record.HSP with many utilities to get correspondance between subject, query and alignment positions.
    """
    
    def __init__(self):
        self.hit_id = None
        self.hit_species = None
        self.strand = self.get_strand
        # aln is used to store an list representation of the alignment of each positon.
        # Example :
        #        alignment on strand -
        #        start residue for the query 5 ; end residue for the query 19 (1 gap in query)
        #        start residue for the subject 48 ; end residue for the subject 52 (1 gap in subject)
        #        3 identities and 4 positives
        #    aln['query']   = [  5,  6,  7,  8,  9,  10,  11,  12,  13, "", "", "", 14, 15, 16, 17, 18, 19]
        #    aln['match']   = [  =,  =,  =, SG, SG,  SG,   =,   =,   =, QG, QG, QG,  =,  =,  =,  +,  +,  +]
        #    aln['subject'] = [ 52, 52, 52, "", "",  "",  51,  51,  51, 50, 50, 50, 49, 49, 49, 48, 48, 48]
        self.aln = { "query":[],
                     "subject":[],
                     "match":[] }

    @staticmethod
    def from_HSP(hsp):
        """
        @summary : Returns an AppHSP from an HSP object.
        @param hsp : [Bio.Blast.Record.HSP] The HSP.
        @return : [AppHSP]
        """
        app_hsp = AppHSP()
        app_hsp.score = hsp.score
        app_hsp.bits = hsp.bits
        app_hsp.expect = hsp.expect
        app_hsp.num_alignments = hsp.num_alignments
        app_hsp.identities = hsp.identities
        app_hsp.positives = hsp.positives
        app_hsp.gaps = hsp.gaps
        app_hsp.align_length = hsp.align_length
        app_hsp.frame = hsp.frame
        app_hsp.strand = app_hsp.get_strand()
        app_hsp.query = hsp.query
        app_hsp.query_start = hsp.query_start
        app_hsp.query_end = hsp.query_end
        app_hsp.match = hsp.match
        app_hsp.sbjct = hsp.sbjct
        app_hsp.sbjct_start = hsp.sbjct_start
        app_hsp.sbjct_end = hsp.sbjct_end
        return app_hsp

    def get_subject_pos(self, aln_pos):
        """
        @summary : Returns the position on subject (in aa) corresponding to position in alignment.
        @param aln_pos : [int] Position in alignment.
        @return : [int]
        """
        return self.aln["subject"][aln_pos]

    def get_query_pos(self, aln_pos):
        """
        @summary : Returns the position on query (in nt) corresponding to position in alignment.
        @param aln_pos : [int] Position in alignment.
        @return : [int]        
        """
        return self.aln["query"][aln_pos]

    def get_aln_pos_from_query(self, query_pos):
        """
        @summary : Returns the position on alignment corresponding to position in query.
        @param query_pos : [int] Position in query (in nt).
        @return : [int]        
        """
        aln_pos = self.aln["query"].index( query_pos )
        if aln_pos == -1:
            raise Exception( "The Query position '" + str(query_pos) + "' is not implicated in the HSP." )
        return aln_pos

    def get_strand(self):
        """"
        @summary : Returns strand of the query alignment.
        @return : [str]
        """
        strand = "+"
        if (self.frame[0] < 0):
            strand = "-"
        return strand
    
    def get_codon_pos(self, aln_pos):
        """
        @summary : Returns the position on codon for the nucleotid in the specified alignment position.
        @param aln_pos : [int] Position in alignment.
        @return : [int]
        @warning : self.sbjct_start must be inferior than self.sbjct_end.
        """
        codon_pos = None
        if aln_pos != (len(self.aln["subject"])-1) and self.aln["subject"][aln_pos] == self.aln["subject"][aln_pos+1]:
            if aln_pos != 0 and self.aln["subject"][aln_pos] == self.aln["subject"][aln_pos-1]:
                codon_pos = 2
            else:
                codon_pos = 1
        else:
            codon_pos = 3
        return codon_pos

    def _tri_pos(self, pos):
        """
        @summary : Returns the three query positions for the codon which strat on pos in stranded alignment.
        @retruns : [list] The three position in order.
        """
        return { 'next':(pos+3), 'codon':[pos, pos+1, pos+2] }

    def _tri_rev_pos(self, pos):
        """
        @summary : Returns the three query positions for the codon which strat on pos in reverse stranded alignment.
        @retruns : [list] The three position in order.
        """
        return { 'next':(pos-3), 'codon':[pos, pos-1, pos-2] }

    def set_pos(self):
        """
        @summary : set aln["query"], aln["match"] and aln["subject"].
        """
        query_pos = None
        codon_positions = None
        subject_pos = self.sbjct_start
        if self.strand == "+":
            query_pos = self.query_start
            codon_positions = self._tri_pos
        else:
            query_pos = self.query_end
            codon_positions = self._tri_rev_pos

        for align_idx, align_char in enumerate( self.match ):
            if align_char == '+': # conservation
                positions = codon_positions(query_pos)
                query_pos = positions["next"]
                self.aln["query"] += positions["codon"]
                self.aln["subject"] += [subject_pos, subject_pos, subject_pos]
                subject_pos += 1
                self.aln["match"] += ["+", "+", "+"]
            elif align_char.isalpha(): # identity
                positions = codon_positions(query_pos)
                query_pos = positions["next"]
                self.aln["query"] += positions["codon"]
                self.aln["subject"] += [subject_pos, subject_pos, subject_pos]
                subject_pos += 1
                self.aln["match"] += ["=", "=", "="] 
            else:
                if self.query[align_idx] == "-": # gap in query
                    self.aln["query"] += ["", "", ""]
                    self.aln["subject"] += [subject_pos, subject_pos, subject_pos]
                    subject_pos += 1
                    self.aln["match"] += ["QG", "QG", "QG"]
                elif self.sbjct[align_idx] == "-": # gap in subject
                    positions = codon_positions(query_pos)
                    query_pos = positions["next"]
                    self.aln["query"] += positions["codon"]
                    self.aln["subject"] += ["", "", ""]
                    self.aln["match"] += ["SG", "SG", "SG"]
                else: # mismatch
                    positions = codon_positions(query_pos)
                    query_pos = positions["next"]
                    self.aln["query"] += positions["codon"]
                    self.aln["subject"] += [subject_pos, subject_pos, subject_pos]
                    subject_pos += 1
                    self.aln["match"] += ["-", "-", "-"]
