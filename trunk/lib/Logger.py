# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


import sys
import time

class Logger:
    """
    @summary : Class to log program execution. You can defined the output stream and the levels managed.
    @todo : Singleton and factory for logger.
    """

    def __init__(self, filepath=None, levels=None, time_function=None):
        """
        @param filepath : [str] The log filepath. [default : STDOUT]
        @param levels : [list] The list of levels managed by logger (ex : "INFO", "DEBUG", "USER" ).
        @param time_function : [func] The function used to display time in log message. [default : time.time]
        """
        self.filepath = filepath
        self.time_function = time_function if time_function is not None else time.time
        self.levels = levels if levels is not None else ["INFO"]
        if self.filepath is not None:
            self.file_handle = open( self.filepath, "a" )
        else:
            self.file_handle = sys.stdout

    def __del__(self):
        """
        @summary : Closed file handler when the logger is detroyed.
        """
        if self.filepath is not None:
            self.file_handle.close()
            self.file_handle = None

    def write(self, msg, level="INFO", section=None):
        """
        @summary : Writes the log message if the logger manage the message level.
        @param msg : [str] The message.
        @param level : [str] Level of the message.
        @param section : [str] The section for the message.
        """
        if level in self.levels:
            if section is None:
                self.file_handle.write( "[" + str(self.time_function()) + "] " + level + " - " + msg + "\n" )
            else:
                self.file_handle.write( "[" + str(self.time_function()) + "] " + level + " - " + section + " : " + msg + "\n" )
