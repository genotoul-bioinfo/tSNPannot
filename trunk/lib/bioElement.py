# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class Sequence:
    def __init__(self, id, string, description=None, quality=None):
        """
        @param id : [str] Id of the sequence.
        @param string : [str] Sequence of the sequence.
        @param description : [str] The sequence description.
        @param quality : [str] The quality of the sequence (same length as string).
        """
        self.id = id
        self.description = description
        self.string = string
        self.quality = quality


class NucleicSequence(Sequence):
    complement_rules = {'A':'T','T':'A','G':'C','C':'G','U':'A','N':'N','W':'W','S':'S','M':'K','K':'M','R':'Y','Y':'R','B':'V','V':'B','D':'H','H':'D',
                        'a':'t','t':'a','g':'c','c':'g','u':'a','n':'n','w':'w','s':'s','m':'k','k':'m','r':'y','y':'r','b':'v','v':'b','d':'h','h':'d'}

    def revcom( self ):
        """
        @summary : Reverse complement the sequence.
        """
        self.string = "".join([NucleicSequence.complement_rules[base] for base in self.string[::-1]])

    def complement( self ):
        """
        @summary : Complement the sequence.
        """
        self.string = "".join([NucleicSequence.complement_rules[base] for base in list(self.string)])


class Codon(NucleicSequence):
    genetic_code = {'GTA':'Val', 'GTC':'Val', 'GTG':'Val', 'GTT':'Val', 'GTN':'Val', 'GTW':'Val', 'GTS':'Val', 'GTM':'Val', 'GTK':'Val', 'GTR':'Val', 'GTY':'Val', 'GTB':'Val', 'GTD':'Val', 'GTH':'Val', 'GTV':'Val', 'GUA':'Val', 'GUC':'Val', 'GUG':'Val', 'GUU':'Val', 'GUN':'Val', 'GUW':'Val', 'GUS':'Val', 'GUM':'Val', 'GUK':'Val', 'GUR':'Val', 'GUY':'Val', 'GUB':'Val', 'GUD':'Val', 'GUH':'Val', 'GUV':'Val',
                    'TGC':'Cys', 'TGT':'Cys', 'TGY':'Cys', 'UGC':'Cys', 'UGU':'Cys', 'UGY':'Cys',
                    'GAC':'Asp', 'GAT':'Asp', 'GAU':'Asp', 'GAY':'Asp',
                    'TAA':'*', 'TAG':'*', 'TAR':'*', 'TGA':'*', 'TRA':'*', 'UAA':'*', 'UAG':'*', 'UAR':'*', 'UGA':'*', 'URA':'*',
                    'TTC':'Phe', 'TTT':'Phe', 'TTY':'Phe', 'UUC':'Phe', 'UUU':'Phe', 'UUY':'Phe',
                    'ATG':'Met', 'AUG':'Met',
                    'CTA':'Leu', 'CTC':'Leu', 'CTG':'Leu', 'CTT':'Leu', 'CTN':'Leu', 'CTW':'Leu', 'CTS':'Leu', 'CTM':'Leu', 'CTK':'Leu', 'CTR':'Leu', 'CTY':'Leu', 'CTB':'Leu', 'CTD':'Leu', 'CTH':'Leu', 'CTV':'Leu', 'CUA':'Leu', 'CUC':'Leu', 'CUG':'Leu', 'CUU':'Leu', 'CUN':'Leu', 'CUW':'Leu', 'CUS':'Leu', 'CUM':'Leu', 'CUK':'Leu', 'CUR':'Leu', 'CUY':'Leu', 'CUB':'Leu', 'CUD':'Leu', 'CUH':'Leu', 'CUV':'Leu', 'TTA':'Leu', 'TTG':'Leu', 'TTR':'Leu', 'UUA':'Leu', 'UUG':'Leu', 'UUR':'Leu', 'YTA':'Leu', 'YTG':'Leu', 'YTR':'Leu', 'YUA':'Leu', 'YUG':'Leu', 'YUR':'Leu',
                    'AAC':'Asn', 'AAT':'Asn', 'AAU':'Asn', 'AAY':'Asn',
                    'TAC':'Tyr', 'TAT':'Tyr', 'TAY':'Tyr', 'UAC':'Tyr', 'UAU':'Tyr', 'UAY':'Tyr',
                    'ATA':'Ile', 'ATC':'Ile', 'ATT':'Ile', 'ATW':'Ile', 'ATM':'Ile', 'ATY':'Ile', 'ATH':'Ile', 'AUA':'Ile', 'AUC':'Ile', 'AUU':'Ile', 'AUW':'Ile', 'AUM':'Ile', 'AUY':'Ile', 'AUH':'Ile',
                    'CAA':'Gln', 'CAG':'Gln', 'CAR':'Gln',
                    'ACA':'Thr', 'ACC':'Thr', 'ACG':'Thr', 'ACT':'Thr', 'ACU':'Thr', 'ACN':'Thr', 'ACW':'Thr', 'ACS':'Thr', 'ACM':'Thr', 'ACK':'Thr', 'ACR':'Thr', 'ACY':'Thr', 'ACB':'Thr', 'ACD':'Thr', 'ACH':'Thr', 'ACV':'Thr',
                    'GGA':'Gly', 'GGC':'Gly', 'GGG':'Gly', 'GGT':'Gly', 'GGU':'Gly', 'GGN':'Gly', 'GGW':'Gly', 'GGS':'Gly', 'GGM':'Gly', 'GGK':'Gly', 'GGR':'Gly', 'GGY':'Gly', 'GGB':'Gly', 'GGD':'Gly', 'GGH':'Gly', 'GGV':'Gly',
                    'CAC':'His', 'CAT':'His', 'CAU':'His', 'CAY':'His',
                    'TGG':'Trp', 'UGG':'Trp',
                    'GAA':'Glu', 'GAG':'Glu', 'GAR':'Glu',
                    'AGC':'Ser', 'AGT':'Ser', 'AGU':'Ser', 'AGY':'Ser', 'TCA':'Ser', 'TCC':'Ser', 'TCG':'Ser', 'TCT':'Ser', 'TCN':'Ser', 'TCW':'Ser', 'TCS':'Ser', 'TCM':'Ser', 'TCK':'Ser', 'TCR':'Ser', 'TCY':'Ser', 'TCB':'Ser', 'TCD':'Ser', 'TCH':'Ser', 'TCV':'Ser', 'UCA':'Ser', 'UCC':'Ser', 'UCG':'Ser', 'UCU':'Ser', 'UCN':'Ser', 'UCW':'Ser', 'UCS':'Ser', 'UCM':'Ser', 'UCK':'Ser', 'UCR':'Ser', 'UCY':'Ser', 'UCB':'Ser', 'UCD':'Ser', 'UCH':'Ser', 'UCV':'Ser',
                    'CCA':'Pro', 'CCC':'Pro', 'CCG':'Pro', 'CCT':'Pro', 'CCU':'Pro', 'CCN':'Pro', 'CCW':'Pro', 'CCS':'Pro', 'CCM':'Pro', 'CCK':'Pro', 'CCR':'Pro', 'CCY':'Pro', 'CCB':'Pro', 'CCD':'Pro', 'CCH':'Pro', 'CCV':'Pro',
                    'GCA':'Ala', 'GCC':'Ala', 'GCG':'Ala', 'GCT':'Ala', 'GCU':'Ala', 'GCN':'Ala', 'GCW':'Ala', 'GCS':'Ala', 'GCM':'Ala', 'GCK':'Ala', 'GCR':'Ala', 'GCY':'Ala', 'GCB':'Ala', 'GCD':'Ala', 'GCH':'Ala', 'GCV':'Ala',
                    'AGA':'Arg', 'AGG':'Arg', 'AGR':'Arg', 'CGA':'Arg', 'CGC':'Arg', 'CGG':'Arg', 'CGT':'Arg', 'CGU':'Arg', 'CGN':'Arg', 'CGW':'Arg', 'CGS':'Arg', 'CGM':'Arg', 'CGK':'Arg', 'CGR':'Arg', 'CGY':'Arg', 'CGB':'Arg', 'CGD':'Arg', 'CGH':'Arg', 'CGV':'Arg', 'MGA':'Arg', 'MGG':'Arg', 'MGR':'Arg',
                    'AAA':'Lys', 'AAG':'Lys', 'AAR':'Lys'}

    def trad( self ):
        """
        @summary : Returns the 3 letters amino acid code for the codon or '?' if the codon is ambiguous.
        @return : [str]
        """
        try:
            return Codon.genetic_code[self.string.upper()]
        except:
            return '?'

    def __str__(self):
        return self.string
