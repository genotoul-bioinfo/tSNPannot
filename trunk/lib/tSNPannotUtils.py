# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


from gff3 import *
from bioElement import *

#######################################################################################
#
#                                FUNCTIONS
#
#######################################################################################
def is_filtered( hsp, min_identity, min_conserv, min_conserved_border, variant_position ):
    """
    @summary : Return True if the HSP fail filter evaluation.
    @param hsp : [AppHSP] The HSP to evaluate.
    @param min_identity : [float] Minimum identity ratio to keep HSP.
    @param min_conserv : [float] Minimum positive ratio to keep HSP.
    @param min_conserved_border : [int] Minimum positive length before and after variant aa (in aa).
    @param variant_position : [int] The variant position in query.
    @return : [bool]
    """
    is_filtered = True
    if float(hsp.identities)/hsp.align_length >= min_identity and float(hsp.positives)/hsp.align_length >= min_conserv:
        if variant_position >= hsp.query_start and variant_position <= hsp.query_end:
            hsp.set_pos()
            aln_pos = hsp.get_aln_pos_from_query( variant_position )
            if hsp.aln["match"][aln_pos] != "SG" and (min_conserved_border == 0 or conserv_area(hsp, aln_pos, min_conserved_border) ):
                is_filtered = False
    return is_filtered
    
def conserv_area( hsp, aln_pos, size=2 ):
    """
    @summary : Return True if the HSP succeed positive area evaluation.
    @param hsp : [AppHSP] The HSP to evaluate.
    @param min_conserved_border : [int] Minimum positive length before and after variant aa (in aa).
    @param variant_position : [int] The variant position in query.
    @return : [bool]
    """
    is_conserved = True
    codon_pos = hsp.get_codon_pos( aln_pos )
    codon_before_end = aln_pos - codon_pos
    codon_after_start = aln_pos + (3 - codon_pos) + 1
    before_start = codon_before_end - (size*3) + 1
    after_end = codon_after_start + (size*3) - 1
    if before_start >= 0 and after_end < len(hsp.aln["match"]):
        aln_before = hsp.aln["match"][before_start:codon_before_end+1]
        aln_after  = hsp.aln["match"][codon_after_start:after_end+1]
        if "-" in aln_before or "-" in aln_after or "SG" in aln_before or "SG" in aln_after or "QG" in aln_before or "QG" in aln_after:
            is_conserved = False
    else:
        is_conserved = False
    return is_conserved

def load_known_variants( filepath ):
    """
    @summary : Return variants from VCF.
    @param filepath : [str] Path to the VCF file.
    @return : [dict] {Region_name:{SNP_position:[SNP_id]}}.
    """
    known = dict()
    FH = open( filepath )
    for line in FH:
        if not line.startswith("#"):
            fields = line.strip().split()
            if not known.has_key( fields[0] ):
                known[fields[0]] = dict()
            if not known[fields[0]].has_key( fields[1] ):
                known[fields[0]][fields[1]] = list()
            known[fields[0]][fields[1]].append(fields[2])
    FH.close()
    return known

def protein_mut_consequence( ref_aa, mut_aa ):
    """
    @summary : Return the SO consequence of the aa mutation or 'unknown'.
    @param ref_aa : [str] The reference amino acid.
    @param mut_aa : [str] The mutated amino acid.
    @See : SO:0001580 children.
    """
    consequence = None
    if ref_aa == '?' or mut_aa == '?':
        consequence = "unknown"
    elif ref_aa == mut_aa:
        consequence = "synonymous_variant"
    elif ref_aa != "*" and mut_aa != "*":
        consequence = "missence_variant"
    elif mut_aa == "*":
        consequence = "stop_gained"
    else:
        consequence = "stop_lost"
    return consequence


#######################################################################################
#
#                                STORAGE
#
#######################################################################################
class AnnotSequence(Sequence):
    def __init__(self, id, string, description=None, quality=None):
        """
        @param id : [str] Id of the sequence.
        @param string : [str] Sequence of the sequence.
        @param description : [str] The sequence description.
        @param quality : [str] The quality of the sequence (same length as string).
        """
        Sequence.__init__(self, id, string, description, quality)
        self.variant = None # Object variant
        self.hsps = list() # HSP after filtering
        self.annotations = list() # Annotations of the variant for each self.hsps
        
    def annot_to_gff(self):
        """
        @summary : Returns a generator on list of annotations.
        @return : [Generator] List of GFF3Record.
        """
        for idx, curr_annot in enumerate(self.annotations):
            annot = GFF3Record()
            annot.seq_id = str(self.id)
            annot.source = "tSNPannot"
            annot.type = "SNP"
            annot.start = self.variant.ref_pos
            annot.end = self.variant.ref_pos
            annot.score = self.hsps[idx].score
            annot.strand = self.hsps[idx].strand
            annot.phase = "*"
            annot.attributes = dict()
            annot.setAttribute( "variant_id", self.variant.id )
            # Alignment scores
            annot.setAttribute( "identities", self.hsps[idx].identities )
            annot.setAttribute( "positives", self.hsps[idx].positives )
            annot.setAttribute( "align_length", self.hsps[idx].align_length ) # subject end - subject start + gap in query + gap in subject
            # Target information
            annot.setAttribute( "Target", self.hsps[idx].hit_id + " " + str(self.hsps[idx].sbjct_start) + " " + str(self.hsps[idx].sbjct_end) + " " + self.hsps[idx].strand )
            annot.setAttribute( "species", self.hsps[idx].hit_species )
            annot.setAttribute( "ref_region", self.annotations[idx].ref_region_name )
            annot.setAttribute( "ref_position", self.annotations[idx].ref_region_position )
            annot.setAttribute( "exon_5prim_barrier", self.annotations[idx].exon_5prim_barrier )
            annot.setAttribute( "exon_3prim_barrier", self.annotations[idx].exon_3prim_barrier )
            annot.setAttribute( "gene_name", self.annotations[idx].gene_name )
            if self.annotations[idx].known_variants:
                annot.setAttribute( "known_variants", ','.join(self.annotations[idx].known_variants) )
            # Alleles consequences
            if self.hsps[idx].strand == "+":
                if self.annotations[idx].codon_pos == 3:
                    before = self.variant.before[-2] + self.variant.before[-1]
                    after = ""
                elif self.annotations[idx].codon_pos == 2:
                    before = self.variant.before[-1]
                    after = self.variant.after[0]
                else:
                    before = ""
                    after = self.variant.after[0] + self.variant.after[1]
            else:
                if self.annotations[idx].codon_pos == 3:
                    before = self.variant.after[1] + self.variant.after[0]
                    after = ""
                elif self.annotations[idx].codon_pos == 2:
                    before = self.variant.after[0]
                    after = self.variant.before[-1]
                else:
                    before = ""
                    after = self.variant.before[-1] + self.variant.before[-2]
            ref_codon = None
            for allele_idx, allele in enumerate(self.variant.alleles):
                codon = Codon( 'noId', before + allele + after )
                if self.hsps[idx].strand == "-":
                    codon.complement()
                annot.addToAttribute( "codon_consequences", codon.string )
                current_aa = Codon.trad(codon)
                annot.addToAttribute( "aa_consequences", current_aa )
                if allele_idx != 0: # mutated codon
                    annot.addToAttribute( "protein_consequences", protein_mut_consequence( ref_aa, current_aa ) )
                else: # ref codon
                    ref_aa = current_aa
                    annot.addToAttribute( "protein_consequences", "-" )
            yield annot
      
    def __str__(self):
        return ">" + self.id + " variant[" + str(self.variant) + "]\n" + self.string

 
class Variant:
    def __init__(self, id, pos, alleles=None, carrier=None ):
        """
        @id : [str] The identifiant of the variation.
        @pos : [int] The position position on carrier.
        @alleles : [list] The list of alleles (ref allele in first).
        @carrier : [AnnotSequence] The sequence where the variant is.
        """
        self.id = id
        self.pos = pos # position on seq
        self.ref_pos = None # position on real ref
        self.carrier = carrier
        self.alleles = alleles
        self.before = "" # use instead of AnnotSequence.string to reduce memory consumption
        self.after = "" # use instead of AnnotSequence.string to reduce memory consumption
    
    def __str__(self):
        return self.id + " " + self.before + "[" + "/".join(self.alleles) + "]" + self.after + " in " + self.carrier.id + ":" + str(self.pos)


class Annotation:
    def __init__( self, ref_region_name, ref_region_position, ref_strand, exon_5prim_barrier, exon_3prim_barrier, codon_pos, gene_name=None ):
        """
        @ref_region_name : [str] The name of the region/chromosome where the variant is find on reference species.
        @ref_region_position : [str] The position on the reference species region where the variant is find on reference species.
        @ref_strand : [str] The strand of the RNA where the variant is find on reference species.
        @exon_5prim_barrier : [int] Nucleic distance between variant and five prime exon start.
        @exon_3prim_barrier : [int] Nucleic distance between variant and three prime exon end.
        @codon_pos : [int] Position of variant in codon.
        @gene_name : [str] Hit gene name.
        """
        self.ref_region_name = ref_region_name
        self.ref_region_position = ref_region_position
        self.ref_strand = ref_strand
        self.exon_5prim_barrier = exon_5prim_barrier
        self.exon_3prim_barrier = exon_3prim_barrier
        self.codon_pos = codon_pos
        self.gene_name = gene_name
        self.known_variants = None
