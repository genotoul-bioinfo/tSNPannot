# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class VCFRecord:
    def __init__(self, region=None, position=None, knownSNPId=None, refAllele=None, altAlleles=None, qual=None, pFilter=None, info=None, pFormat=None, samples=None):
        self.chrom   = region
        self.pos     = position
        self.id      = knownSNPId
        self.ref     = refAllele
        self.alt     = altAlleles
        self.qual    = qual
        self.filter  = pFilter
        self.info    = info if info is not None else dict()
        self.format  = pFormat if pFormat is not None else list()
        self.samples = samples if samples is not None else list()
    
    def isIndel(self):
        """
        @summary : Return True if the variant is an insertio or a deletion.
        @return : [bool]
        """
        isIndel = False
        if self.ref == "." or self.ref == "-":
            isIndel = True
        else:
            for allele in self.alt:
                if len(allele) != len(self.ref) or allele == "." or allele == "-":
                    isIndel = True
        return isIndel
        
    def type(self):
        """
        @summary : Returns the vrariant type.
        @return : [str] 'snp' or 'indel' or 'variation'.
        """
        record_type = "snp"
        if self.isIndel():
            record_type = "indel"
        elif len(self.ref) > 1:
            record_type = "variation"
        return record_type


class VCFIO:
    def __init__( self, filepath, mode="r" ):
        """
        @param filepath : [str] The filepath.
        @param mode : [str] Mode to open the file ('r', 'w', 'a').
        """
        self.filepath = filepath
        self.mode = mode
        self.file_handle = open( filepath, mode )
        self.current_line_nb = 0
        self.current_line = None
    
    def __del__( self ):
        self.close()
    
    def __iter__( self ):
        for line in self.file_handle:
            self.current_line = line.rstrip()
            self.current_line_nb += 1
            if self.current_line.startswith('#') :
                continue
            try:
                vcf_record = self._parse_line()
            except:
                raise IOError( "The line " + str(self.current_line_nb) + " in '" + self.filepath + "' cannot be parsed by " + self.__class__.__name__ + ".\n" +
                               "Line content : " + self.current_line )
            else:
                yield vcf_record
            
    def close( self ) :
        if hasattr(self, 'file_handle') and self.file_handle is not None:
            self.file_handle.close()
            self.file_handle = None
            self.current_line_nb = None
            self.current_line = None
    
    def _parse_line( self ):
        """
        @summary : Returns a structured record from the VCF current line.
        @return : [VCFRecord]
        """
        fields = self.current_line.rstrip().split('\t')
        variation = VCFRecord() 
        variation.chrom  = fields[0]
        variation.pos    = int(fields[1])
        variation.id     = fields[2]
        variation.ref    = fields[3]
        variation.alt    = fields[4].split(',')
        variation.qual   = fields[5]
        variation.filter = fields[6]
        variation.format = fields[8].split(':')

        # Field INFO
        if fields[7] != '.' :
            info = dict()
            for tag_and_value in fields[7].split(';'):
                if "=" in tag_and_value:
                    tag, value = tag_and_value.split('=')
                    if "," in value:
                        info[tag] = value.split(",")
                    else:
                        info[tag] = value
                else:
                    info[tag_and_value] = True
            variation.info = info
        #~ # Fields samples
        #~ regexp_none=re.compile("\.(\/\.)*")
        #~ for lib_infos in range (9,len(fields)) :
            #~ if not regexp_none.match(fields[lib_infos]):
                #~ sformat = fields[lib_infos].split(':')
                #~ variation.samples.append( Entry(**{ autocast(variation.format[i]) : autocast(sformat[i]) if sformat[i] != '.' else None for i in range(0,len(variation.format)) } )  )
            #~ else :
                #~ variation.samples.append( Entry(**{ autocast(variation.format[i]) : None for i in range(0,len(variation.format)) }) )
        return variation
