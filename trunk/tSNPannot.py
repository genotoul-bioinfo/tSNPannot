#!/usr/bin/env python2.7
# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '2.0.2'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
import sys
import argparse

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.dirname(CURRENT_DIR))
sys.path.insert(0, os.path.join(CURRENT_DIR, "lib"))
import NCBIXML
from Record import HSP
from AppHSP import AppHSP
from gtf import GTFI
from fasta import FastaIO
from vcf import *
from gff3 import *
from tSNPannotUtils import *
from bioStructure import *
from bioElement import *
from Logger import Logger


class AnnotDbParameter(argparse.Action):
    """
    @summary : Argparse parameter fo annotation databanks.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        valid_param = ["species", "aln", "gtf", "vcf"]
        required_param = ["species", "aln", "gtf"]
        # Retrieve params
        current_annot_db = dict()
        for option in values:
            param, val = option.split("=")
            param = param.strip()
            val = val.strip()
            if not param in valid_param:
                parser.error("Unknown sub-parameter '" + param + "' in argument " + option_string + ".")
            current_annot_db[param] = val
        absents = list()
        for required in required_param:
            if not required in current_annot_db.keys():
                absents.append(required)
        if len(absents) > 0:
            parser.error("Missing sub-parameter '" + "', ".join(absents) + "' in '" + option_string + " " + " ".join(values) + "'.")
        # Set parser
        annotations_db = getattr(namespace, self.dest)
        if annotations_db is None:
            annotations_db = list()
        annotations_db.append( current_annot_db )
        setattr(namespace, self.dest, annotations_db) 


if __name__ == "__main__":
    logger = Logger()

    # Manage parameters
    parser = argparse.ArgumentParser( description='Annotates by similarities the SNPs called from RNSeq de novo data.' )
    parser.add_argument( "--version", action='version', version=__version__ )
    group_input_output = parser.add_argument_group( 'Inputs/outputs' ) # Input/outputs
    group_input_output.add_argument( '--sequences-file', type=str, dest='reference_file', required=True, help='Reference sequences (format : FASTA).' )
    group_input_output.add_argument( '--variants-file', type=str, dest='variants_file', required=True, help='Variants to annotate (format : VCF).' )
    group_input_output.add_argument( '--output', type=str, required=True, help='The GFF3 output file.' )
    group_annot_db = parser.add_argument_group( 'Annotation databanks' ) # Annotation databanks
    group_annot_db.add_argument( '--annot-db', dest='annotation_databanks', required=True, action=AnnotDbParameter, nargs='+', metavar=("species=NAME aln=FILEPATH gtf=FILEPATH [vcf=FILEPATH]", ""), help='Databanks used to annotate by similarities.' )
    group_filters = parser.add_argument_group( 'Filters' ) # Parameters used to filter HSP used in annotation
    group_filters.add_argument( '-i', '--min-identity', type=float, default=0.90, help='Use only HSPs with at least this identity.' )
    group_filters.add_argument( '-c', '--min-conserv', type=float, default=0.95, help='Use only HSPs with at least this conservation.' )
    group_filters.add_argument( '-b', '--min-conserv-border', type=int, default=2, help='Use only HSPs with this number of amino acids conserved before and after the amino acid containing the variant.' )
    group_filters.add_argument( '-n', '--nb-annot', type=int, default=1, help='Maximum number of different scores.' )
    args = parser.parse_args()

    # Load variants
    logger.write( "Loading variants." )
    nb_variants = 0
    queries_sequences = dict()
    FH_reference = FastaIO( args.reference_file )
    FH_variants_to_annot = VCFIO( args.variants_file )
    for variant in FH_variants_to_annot:
        nb_variants += 1
        if variant.type() == "snp":
            current_seq = FH_reference.next_seq()
            seq = AnnotSequence( variant.chrom, "" )
            alleles = [variant.ref] + variant.alt
            sub_seq_pos = int(current_seq.description.split(':')[1])
            variant_id = "snp_" + str(nb_variants)
            if variant.id != ".":
                variant_id = variant.id
            seq.variant = Variant( variant_id, sub_seq_pos, alleles, seq )
            seq.variant.ref_pos = variant.pos
            seq.variant.before = current_seq.string[max(sub_seq_pos-5, 0):sub_seq_pos-1]
            seq.variant.after = current_seq.string[sub_seq_pos:min(sub_seq_pos+4, len(current_seq.string)-1)]
            queries_sequences[variant.chrom + "_chr@" + str(variant.pos) + '.' + '_'.join(variant.alt)] = seq
    FH_variants_to_annot.close()
    FH_reference.close()

    # Filter hsp
    logger.write( "Start filtering." )
    for curr_annot_db in args.annotation_databanks:
        blast_fh = open( curr_annot_db["aln"] )
        blast_records = NCBIXML.parse( blast_fh )
        for blast_record in blast_records:
            query_id = blast_record.query.split()[0]
            for alignment in blast_record.alignments:
                for hsp in alignment.hsps:
                    app_hsp = AppHSP.from_HSP(hsp)
                    app_hsp.hit_id = alignment.hit_def.split()[0]
                    if not is_filtered( app_hsp, args.min_identity, args.min_conserv, args.min_conserv_border, queries_sequences[query_id].variant.pos ):
                        query = queries_sequences[query_id]
                        app_hsp.hit_species = curr_annot_db["species"]
                        if len(query.hsps) < args.nb_annot:
                            query.hsps.append( app_hsp )
                            query.annotations.append( None )
                            query.hsps = sorted(query.hsps, key=lambda hsp: -hsp.score) #sort by decreasing score
                        elif query.hsps[-1].score < app_hsp.score:
                            query.hsps.append( app_hsp )
                            query.hsps = sorted(query.hsps, key=lambda hsp: -hsp.score) #sort by decreasing score
                            nb_diff = 0
                            prev_score = None
                            stop_idx = len(query.hsps)
                            for idx, hsp in enumerate(query.hsps):
                                if prev_score != hsp.score:
                                    prev_score = hsp.score
                                    nb_diff += 1
                                    if nb_diff == args.nb_annot + 1:
                                        stop_idx = idx
                            query.hsps = query.hsps[0:stop_idx]
                            query.annotations = [None for pos in range(0,stop_idx)]
        blast_fh.close()

    # Annotate variants
    for curr_annot_db in args.annotation_databanks:
        logger.write( "Process position in '" + curr_annot_db["species"] + "'." )
        logger.write( "Load '" + curr_annot_db["species"] + "' GTF.", "INFO", "Position annotation" )
        # Find variant subject position
        transcripts_model, proteins_model = GTFI( curr_annot_db["gtf"] ).load_model()
        logger.write( "Load '" + curr_annot_db["species"] + "' GTF OK.", "INFO", "Position annotation" )
        for query in queries_sequences.values():
            for idx, hsp in enumerate(query.hsps):
                # Filter by species
                if hsp.hit_species == curr_annot_db["species"]:
                    # Subject position
                    aln_pos = hsp.get_aln_pos_from_query( query.variant.pos )
                    subject_pos = hsp.get_subject_pos( aln_pos )
                    codon_pos = hsp.get_codon_pos( aln_pos )
                    # Genome position
                    subject_protein = proteins_model[hsp.hit_id]
                    cds, cds_pos = subject_protein.getEltByPos( subject_pos, codon_pos )
                    dna_region_pos = cds.getPosOnRef( cds_pos )
                    dna_region_name = cds.reference.name
                    # Exon barrier distance
                    transcript = transcripts_model[subject_protein.transcript.id]
                    exon, exon_pos = transcript.getEltByPos( subject_protein.getPosOnGlobalCDS(subject_pos, codon_pos) + subject_protein.getFivePrimUTR() )
                    if exon.strand == "+":
                        exon_5prim_barrier = exon_pos - 1
                        exon_3prim_barrier = exon.length() - exon_pos
                    else:
                        exon_5prim_barrier = exon.length() - exon_pos
                        exon_3prim_barrier = exon_pos - 1
                    query.annotations[idx] = Annotation( dna_region_name, dna_region_pos, transcript.strand, exon_5prim_barrier, exon_3prim_barrier, codon_pos, transcript.gene.name )
        del transcripts_model
        del proteins_model

        # Known variants
        if curr_annot_db.has_key( "vcf" ):
            logger.write( "Find Known variants in '" + curr_annot_db["species"] + "'." )
            logger.write( "Load '" + curr_annot_db["species"] + "' VCF.", "INFO", "Known variants" )
            known_variants = load_known_variants( curr_annot_db["vcf"] )
            logger.write( "Load '" + curr_annot_db["species"] + "' VCF OK.", "INFO", "Known variants" )
            for query in queries_sequences.values():
                for idx, hsp in enumerate(query.hsps):
                    # Filter by species
                    if hsp.hit_species == curr_annot_db["species"]:
                        annotation = query.annotations[idx]
                        if known_variants.has_key(annotation.ref_region_name) and known_variants[annotation.ref_region_name].has_key(str(annotation.ref_region_position)):
                            annotation.known_variants = known_variants[annotation.ref_region_name][str(annotation.ref_region_position)]
            del known_variants

    # Write output
    logger.write( "Write outpout." )
    annotation_file = GFF3IO( args.output, "w" )
    annotation_file._handle.write( "## command : " + " ".join(sys.argv) + "\n" )
    annotation_file._handle.write( "## version : " + __version__ + "\n" )
    for query in queries_sequences.values():
        for curr_annot in query.annot_to_gff():
            annotation_file.write( curr_annot )

    logger.write( "Execution complete." )
