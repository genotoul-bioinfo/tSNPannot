#!/usr/bin/env python2.7
# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
import sys
import argparse
import subprocess

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.dirname(CURRENT_DIR))
sys.path.insert(0, os.path.join(CURRENT_DIR, "lib"))
from fasta import *
from vcf import *
from Logger import Logger


if __name__ == "__main__":
	logger = Logger()
	
	# Manage parameters
	parser = argparse.ArgumentParser( description='Extract sub-sequences centered on vrariants and blast these sequences on databank.' )
	parser.add_argument( '-u', '--max-upstream', type=int, default=90, help='Maximum number of nucleotids before variant in the new variant seq.' )
	parser.add_argument( '-d', '--max-downstream', type=int, default=90, help='Maximum number of nucleotids after variant in the new variant seq.' )
	parser.add_argument( "--version", action='version', version=__version__ )
	group_input = parser.add_argument_group( 'Inputs' ) # Inputs
	group_input.add_argument( '--sequences-file', required=True, help='Reference sequences (format : FASTA).' )
	group_input.add_argument( '--variants-file', required=True, help='Variants to annotate (format : VCF).' )
	group_blast = parser.add_argument_group( 'Alignment options' ) # Alignments
	group_blast.add_argument( '--aln-db', required=True, help='Databank used to find similarities.' )
	group_blast.add_argument( '-b', '--blast-path', required=True, help='The blast program path.' )
	group_blast.add_argument( '-t', '--nb-threads', type=int, default=2, help='Number of threads.' )
	group_output = parser.add_argument_group( 'Outputs' ) # Outputs
	group_output.add_argument( '-q', '--queries-file', required=True, help='The new sequences file (short sequences centered on variants).')
	group_output.add_argument( '-x', '--blast-file', required=True, help='The blast output file path.')
	args = parser.parse_args()

	# Convert variants to fasta
	logger.write( "Variants to queries." )
	queries_file = FastaIO( args.queries_file, "w" )
	sequences_file = FastaIO( args.sequences_file )
	current_seq = sequences_file.next_seq()
	vcf_to_annot = VCFIO( args.variants_file )
	for variant in vcf_to_annot:
		if variant.type() == "snp":
			while current_seq.id != variant.chrom:
				current_seq = sequences_file.next_seq()
			sub_seq_start = max( variant.pos-args.max_upstream-1, 0 )
			sub_seq_end = min( variant.pos+args.max_downstream, len(current_seq.string) )
			sub_seq_str = current_seq.string[sub_seq_start:sub_seq_end+1]
			sub_seq = Sequence( variant.chrom + "_chr@" + str(variant.pos) + '.' + '_'.join(variant.alt), sub_seq_str, "sub@:" + str(variant.pos - sub_seq_start) )
			queries_file.write( sub_seq )
	queries_file.close()

	# Launch blast
	command_line_options = " -db " + args.aln_db + " -query " + args.queries_file + " -outfmt 5 -num_threads " + str(args.nb_threads)
	logger.write( "Align queries : " + args.blast_path + command_line_options + " > " + args.blast_file )
	FH_output = open( args.blast_file, "w" )
	subprocess.check_call( [args.blast_path] + command_line_options.split(), stdout=FH_output )

	logger.write( "Execution complete." )
